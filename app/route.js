import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthStack from './route.auth';
import MainNavigator from './route.drawer';
import RootStack from "./route.app"

const switch_navigator = createSwitchNavigator(
  {
    AuthStack: AuthStack,
     MainNavigator: MainNavigator,
     RootStack:RootStack,

  },
  {
    initialRouteName: "AuthStack"
  },
);

export default createAppContainer(switch_navigator);
