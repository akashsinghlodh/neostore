import {createDrawerNavigator} from 'react-navigation-drawer';
import MyCart from './screen/MyCart';
import MyAccount from './screen/MyAccount';
import StoreLocator from './screen/StoreLocator';
import DrawerComponent from './drawerComponent';
import RootStack from "./route.app"

const MainNavigator = createDrawerNavigator(
    {
      screening:{
        screen: RootStack,
        navigationOptions: {
           drawerLabel: () => null
        }
      },
      MyCart: {
        screen: MyCart,
        navigationOptions: {
          drawerLabel: "My Cart"
        }
      },
      MyAccount: {
        screen: MyAccount,
        navigationOptions: {
          drawerLabel: "My Account"
        }
      },
      StoreLocator: {
        screen: StoreLocator,
        navigationOptions: {
          drawerLabel: "Store Locator"
        }
      },
      MyOrders: {
        screen: StoreLocator,
        navigationOptions: {
          drawerLabel: "My Orders"
        }
      },
      
    },
    {
      drawerBackgroundColor: '#fff',
      activeTintColor: '#fff',
      contentComponent: DrawerComponent,
      contentOptions: {
        inactiveTintColor: '#fff',
        activeTintColor: '#fff',
        itemsContainerStyle: {
          marginVertical: 0,
        },
      },
    },
  );
export default MainNavigator;  