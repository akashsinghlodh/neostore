import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import ProductDetails from './screen/Product_Details';
import Home from './screen/Home';
import MyCart from './screen/MyCart';
import MyAccount from './screen/MyAccount';
import StoreLocator from './screen/StoreLocator';
import MyOrders from './screen/MyOrders';
import ResetPassword from './screen/ResetPassword';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AddAddress from './screen/AddAddress';
import AddressList from './screen/AddressList';
import EditProfile from './screen/EditProfile';
import ProductList from './screen/ProductList';
import Entypo from 'react-native-vector-icons/Entypo';
import OrderID from './screen/OrderID/index';
import ModalScreen from "./components/modalScreen"
const icon_size=25;
const AppNavigator = createStackNavigator(
  {
    OrderID: {
      screen: OrderID,
      navigationOptions: ({navigation}) => ({
        title: navigation.state.params.title,

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    ProductList: {
      screen: ProductList,
      navigationOptions: ({navigation}) => ({
        title: navigation.state.params.title,

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },

    EditProfile: {
      screen: EditProfile,
      navigationOptions: ({navigation}) => ({
        title: 'Edit Profile',

    
      }),
    },

    AddressList: {
      screen: AddressList,
      navigationOptions: ({navigation}) => ({
        title: 'Address List',

        headerRightContainerStyle: {
          paddingRight: 10,
        },
        headerRight: (
          <AntDesign
            onPress={() => {
              navigation.navigate('AddAddress');
            }}
            name="plus"
            size={icon_size}
            color={'#fff'}></AntDesign>
        ),
      }),
    },
    AddAddress: {
      screen: AddAddress,
      navigationOptions: ({navigation}) => ({
        title: 'Add Address',

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    ProductDetails: {
      screen: ProductDetails,
      navigationOptions: ({navigation}) => ({
        title: navigation.state.params.title,
        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    Home: {
      screen: Home,
      navigationOptions: ({navigation}) => ({
        title: 'NeoSTORE',
        headerLeftContainerStyle: {
          paddingLeft: 10,
        },
        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (
          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>
        ),

        headerLeft: (
          <Entypo
            onPress={() => {navigation.toggleDrawer()}}
            name="menu"
            size={icon_size}
            color={'#fff'}></Entypo>
        ),
      }),
    },
    ResetPassword: {
      screen: ResetPassword,
      navigationOptions: ({}) => ({
        title: 'Reset Password',
      }),
    },
    MyCart: {
      screen: MyCart,
      navigationOptions: ({navigation}) => ({
        title: 'My Cart',

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    MyAccount: {
      screen: MyAccount,
      navigationOptions: ({navigation}) => ({
        title: 'My Account',

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    StoreLocator: {
      screen: StoreLocator,
      navigationOptions: ({navigation}) => ({
        title: 'Store Locator',

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>        ),
      }),
    },
    MyOrders: {
      screen: MyOrders,
      navigationOptions: ({navigation}) => ({
        title: 'My Orders',

        headerRightContainerStyle: {
          paddingRight: 10,
        },

        headerRight: (

          <AntDesign
          onPress={() => navigation.navigate('MyModal')}
            name="search1"
            size={icon_size}
            color={'#fff'}></AntDesign>     
               ),
      }),
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#E91C1A',
      },
      headerTitleAllowFontScaling: true,
      headerTintColor: '#fff',
      headerTitleAllowFontScaling: true,
      headerTitleStyle: {
        textAlign: 'center',
        alignSelf: 'center',
        fontSize: 18,
        fontFamily:"Gotham-Medium"

      },
    },
  },
);
const RootStack = createStackNavigator(
  {
    Main: {
      screen:AppNavigator ,
      navigationOptions: ({}) => ({
        header: null,
      }),
    },
    MyModal: {
      screen: ModalScreen,
      navigationOptions: ({}) => ({
        headerStyle: {
          backgroundColor: '#e91c1a',
        },
        headerTitleAllowFontScaling: true,
      headerTintColor: '#fff',
      headerTitleAllowFontScaling: true,
      headerTitleStyle: {
        textAlign: 'center',
        alignSelf: 'center',
        fontSize: 18,
        fontFamily:"Gotham-Medium"

      },
      }),
    },
  },
  {
    mode: 'modal',
  }['default']
);
export default RootStack;
