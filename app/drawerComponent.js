import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import api from './api';
import {DrawerNavigatorItems} from 'react-navigation-drawer';
import parentUrl from './components/parentUrl';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AlertView from './components/alert';
import 'react-native-gesture-handler'
export default class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawer_data: {},
      user_data: {},
      isLoading: false,
      cart_count: '',
      user_email: '',
      user_img: '',
      first_name: '',
      last_name: '',
      entries: [],
    };
  }

  onSuccess = async () => {
    cart = await AsyncStorage.getItem('cart');
    cart = JSON.parse(cart);
    cart = cart == null ? [] : cart;

    if (cart.length > 0) {
      cart.push({flag: 'logout'});

      api
        .apiHttpRequest(
          'addProductToCartCheckout',
          'POST',
          [...cart],
          true,
          true,
        )
        .then(response => {
          if (response.status_code == 200) {
            this.props.navigation.navigate('Login');
            AsyncStorage.removeItem('token');
            AsyncStorage.removeItem('cart');
            AsyncStorage.removeItem('cart_data');
            alert('Log out successful.');
          }
        })
        .catch(error => {
          alert(error);
        });
    } else {
      this.props.navigation.navigate('Login');
      AsyncStorage.removeItem('token');
      AsyncStorage.removeItem('cart');
      AsyncStorage.removeItem('cart_data');
      alert('Log out successful.');
    }
  };

  renderMenuItem = scene => {
    const routeIndex = scene.route.index;
    if (scene.route) {
      let label = this.props.getLabel(scene);
      const color = scene.tintColor;

      return (
        scene.route.key != 'screening' && (
          <View style={styles.navItem}>
            <FontAwesome5
              style={styles.navItemIcon}
              name={iconData[scene.route.key]}
              color={color}
            />
            <View style={styles.drawer_innew_view}>
              <Text style={[styles.navText]}>{label}</Text>
            </View>
            {scene.route.key === 'MyCart' && (
              <View style={styles.my_cart_view}>
                <Text style={styles.cart_count}>{this.state.cart_count}</Text>
              </View>
            )}
          </View>
        )
      );
    }
  };
  render_Screen = () => {
    const {entries} = this.state;
    return Object.keys(entries).length > 0 && entries.map((result, index) => {
      return (
        <TouchableOpacity
        key={result.category_id}
          onPress={() => {
            this.props.navigation.navigate('ProductList', {
              _id: result.category_id,
              title: result.category_name,
            });
          }}
          style={styles.navItem}>
          <FontAwesome5
            style={styles.navItemIcon}
            name={iconData[result.category_name]}
            color={'#fff'}
          />
          <View style={styles.drawer_innew_view}>
            <Text style={styles.navText}> {result.category_name}</Text>
          </View>
        </TouchableOpacity>
      );
    });
  };
  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      const cart_count = await AsyncStorage.getItem('cart_count');

      if (value !== null) {
        this.setState({cart_count: cart_count});
        api.flag=false

        api
          .apiHttpRequest('getCustProfile', 'GET', {}, true)
          .then(response => {
            this.setState({isLoading: false});
            if(response.status_code==200){
            this.setState({user_data: response.customer_proile});
            }
            
          })
          .catch(error => error);
      }
    } catch (error) {
      console.log('error=', error);
    }

    api
      .apiHttpRequest('getAllCategories', 'GET', '')
      .then(response => {

        if (response.status_code == 200) {
          this.setState({isLoading: false});

          if(response.status_code==200){

          this.setState({entries: response.category_details});
          }
        }
      })
      .catch(error => error);
  };

  profile_user = () => {
    return (
      Object.keys(this.state.user_data).length > 0 && (
        <SafeAreaView style={styles.dp_safeareaview}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('EditProfile');
            }}
            style={styles.imageContainer}>
            <Image
              style={styles.headerImage}
              source={
                this.state.user_data.profile_img != null
                  ? {
                      uri: `${parentUrl}${(this.state.user_data.profile_img)}`,
                    }
                  : {
                      uri:
                        'https://yt3.ggpht.com/a-/AAuE7mDaG2knmVyICW5ENEMk2tVgDPvMagd6wk0xww=s88-c-k-c0x00ffffff-no-rj-mo',
                    }
              }
              resizeMode="stretch"
            />
          </TouchableOpacity>
          <Text style={styles.dp_name_text}>
            {this.state.user_data.first_name} {this.state.user_data.last_name}
          </Text>
          <Text style={styles.email_text}>{this.state.user_data.email}</Text>
        </SafeAreaView>
      )
    );
  };
  componentDidUpdate(preVprops, preVstate) {
    const prev = preVprops;
    const next = this.props;
    if (prev != next) {
      this._retrieveData();
    }
  }
  componentDidMount() {
    this._retrieveData();

  }

  render() {
    const {user_data} = this.state;

    return (
      <SafeAreaView style={styles.main_drawer}>
        {this.profile_user()}
        <ScrollView>
          <DrawerNavigatorItems
            {...this.props}
            getLabel={this.renderMenuItem}
          />
          {this.render_Screen()}
          <TouchableOpacity
            onPress={() => {
              AlertView(
                'Alert',
                'Ayou sure you want to Logout?',
                this.onSuccess,
              );
            }}
            style={styles.navItem}>
            <FontAwesome5
              style={styles.navItemIcon}
              name={iconData['Logout']}
              color={'#fff'}
            />
            <View style={styles.drawer_innew_view}>
              <Text style={styles.navText}>Logout</Text>
            </View>
           </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  dp_safeareaview: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  login_button: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    color: '#E91C1A',
    backgroundColor: '#fff',
    width: '95%',
    alignSelf: 'center',
  },

  button_text: {
    paddingTop: 5,
    fontSize: 30,
    color: '#E91C1A',
    fontFamily: 'Gotham-Medium',
  },

  main_drawer: {flex: 1, backgroundColor: '#2c2c2c'},
  dp_name_text: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: 'Gotham-Medium',
  },
  email_text: {color: '#fff', alignSelf: 'center', fontFamily: 'Gotham-Book'},
  imageContainer: {
    overflow: 'hidden',
    height: 90,
    width: 90,
    borderRadius: 45,
    borderWidth: 2,
    borderColor: '#fff',
    backgroundColor: '#7f7f7f',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 5,
  },
  drawer_innew_view: {flexDirection: 'row', justifyContent: 'space-between'},
  headerImage: {
    width: 100,
    height: 100,
    alignSelf: 'center',
  },
  my_cart_view: {
    backgroundColor: '#e91c1a',
    height: 30,
    width: 30,
    borderWidth: 1,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '45%',
    marginRight: -5,
  },
  navItem: {
    paddingHorizontal: 20,
    paddingVertical: 17,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cart_count: {color: '#fff', fontFamily: 'Gotham-Book'},
  navItemIcon: {
    fontSize: 18,
    marginRight: 20,
  },
  navText: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'Gotham-Medium',
  },
});

const iconData = {
  MyCart: 'shopping-cart',
  Table: 'table',
  Sofa: 'couch',
  Chair: 'chair',
  Bed: 'bed',
  Almirah: 'box',
  MyAccount: 'user',
  StoreLocator: 'search-location',
  MyOrders: 'clipboard-list',
  Logout: 'sign-out-alt',
};
