import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import AlertView from '../../components/alert';
import Button from '../../components/button';
import api from '../../api';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../../components/loader';
import OfflineNotice from "../../components/offlineNotice"

class AddressList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: [],
      index: 0,
      first_name: '',
      last_name: '',
      isLoading: true,
      product: [],
      address_order_id: '',
      refreshing: false,
      updateList: props.navigation.getParam('updateList', false),
      addressId: null,
    };
  }
  navigate_to = isLoading => {
    AsyncStorage.removeItem('cart');
    AsyncStorage.removeItem('cart_data');

    if (!isLoading) {
      this.props.navigation.navigate('MyOrders');
    }
  };
  order_placed = () => {
    const {product} = this.state;
    const {address_order_id} = this.state;

    if (address_order_id != '') {
      product.push({flag: 'checkout'});
      api
        .apiHttpRequest(
          'addProductToCartCheckout',
          'POST',
          [...product],
          true,
          true,
        )
        .then(response => {
          this.setState({isLoading: false});

          // console.log(response);
          if (response.status_code == 200) {
            this.navigate_to(this.state.isLoading);
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      this.setState({isLoading: false});
      alert('Please select the address.');
    }
  };
  select_address = (index, value) => {
    this.setState({address_order_id: value});
    api
      .apiHttpRequest(
        'updateAddress',
        'PUT',
        {address_id: value, isDeliveryAddress: true},
        true,
      )
      .then(response => console.log(response))
      .catch(error => console.log(error));
  };

  fetchdata = async () => {
    cart = await AsyncStorage.getItem('cart');
    cart = JSON.parse(cart);
    cart = cart == null ? [] : cart;
    // console.log('address list', cart);
    this.setState({product: cart});
    this.setState({isLoading: true});

    api
      .apiHttpRequest('getCustAddress', 'GET', {}, true)
      .then(response => {
        //console.log(response.customer_address)
        if (response.status_code == 200) {
          this.setState({isLoading: false});

          this.setState({address: response.customer_address});
        } else {
          this.setState({isLoading: false});
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
      });
  };
  onSuccess = () => {
    api
      .apiHttpRequest(`deladdress/${this.state.addressId}`, 'DELETE', {}, true)
      .then(response => {
        //console.log(response)
        this.setState({address_order_id: ''});
        this.fetchdata();
      })
      .catch(error => {
        console.log(error);
      });
  };
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps, prevState) {
    const currentProp = this.props.navigation.getParam('updateList');
    const prevProp = prevProps.navigation.getParam('updateList');
    if (currentProp !== prevProp) {
      this.fetchdata();
    }
  }
  componentWillUnmount() {
    this.props.navigation.state.params.re_Render();
  }

  name_user = async () => {
    const first_name = await AsyncStorage.getItem('first_name');
    const last_name = await AsyncStorage.getItem('last_name');
    this.setState({first_name: first_name, last_name: last_name});
  };
  addressList = () => {
    const {address} = this.state;
    this.name_user();
    return address.map((result, index) => {
      return (
        <RadioButton
          value={result.address_id}
          style={styles.radio_button_style}>
          <View style={styles.inner_view}>
            <TouchableOpacity
              style={styles.delete_button_touch}
              onPress={() => {
                this.setState(
                  {
                    addressId: result.address_id,
                  },
                  () => {
                    AlertView(
                      'Alert',
                      'Are you sure you want to delete ?',
                      this.onSuccess,
                    );
                  },
                );
              }}>
              <Text style={styles.delete_button}>
                <Feather name="x" color={'#7f7f7f'} size={20} />
              </Text>
            </TouchableOpacity>

            <View style={styles.bold_text_view}>
              <Text style={styles.bold_text}>
                {this.state.first_name} {this.state.last_name}
              </Text>

              <Text style={styles.address_text}>
                {result.address}, {result.city}, {result.state}-{result.pincode}
                .{result.country}
              </Text>
            </View>
          </View>
        </RadioButton>
      );
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.outer_most_flex}>
        <Toast ref="toast" />
<OfflineNotice/>
        <Loader loading={this.state.isLoading} />
        <StatusBar backgroundColor="#e91c1a" />

        <View style={styles.top_heading_flex}>
          <Text style={styles.shipping_text_top}>Shipping Address</Text>
        </View>
        <View style={styles.address_list}>
          <ScrollView>
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.addressList()}
            />

            <RadioGroup
              color="#333333"
              activeColor="#bfbfbf"
              thickness={3}
              onSelect={(index, value) => {
                this.select_address(index, value);
              }}>
              {this.addressList()}
            </RadioGroup>

            <Button
              styleButton={styles.place_order_button}
              styleButton_text={styles.place_order_button_text}
              disabled={false}
              buttonState_color={'#e91b1a'}
              textState_color={'#fff'}
              onPress={() => {
                this.order_placed();
              }}
              text="PLACE ORDER"
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
export default AddressList;
