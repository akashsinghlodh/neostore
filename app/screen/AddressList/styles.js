import React, {Component} from "react"
import {  Dimensions,
 StyleSheet} from "react-native"
const {width} = Dimensions.get('window');
const width_screen = width-100 ;

const styles = StyleSheet.create({
  delete_button_touch:{ width:30, alignSelf:"flex-end"},
    place_order_button:{
        backgroundColor: '#e91b1a',
        borderRadius: 10,
        height: 60,
        marginVertical:30,
        alignItems: 'center',
        justifyContent: 'center',
      },
      place_order_button_text:{paddingTop:5,fontSize: 30, color: '#fff',fontFamily:"Gotham-Medium"},
      outer_most_flex: {flex: 1},
      top_heading_flex: {flex: 0.1, marginHorizontal: 15, paddingTop: 20},
      address_list: {flex: 0.9, marginHorizontal: 15},
      shipping_text_top:{fontSize: 20,fontFamily:"Gotham-Book"},
      radio_button_style:{alignItems: 'center', marginHorizontal: -5},
      inner_view:{
        borderWidth: 1,
        borderColor: '#bfbfbf',
        marginLeft: 10,
        width:width_screen
      },
      address_Text:{fontSize: 20,    fontFamily:"Gotham-Medium",width:width_screen},
      delete_button:{alignSelf: 'flex-end'},
      bold_text_view:{paddingHorizontal: 15, paddingBottom: 15},
      bold_text:{fontSize: 25,fontFamily:"Gotham-Medium"},

})
export default styles;