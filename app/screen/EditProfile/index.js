import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  KeyboardAvoidingView,
  ImageBackground,
  StatusBar,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import styles from './styles';
import InputField from '../../components/inputfield';
import Button from '../../components/button';
import api from '../../api';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Loader from '../../components/loader';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import parentUrl from '../../components/parentUrl';
import OfflineNotice from "../../components/offlineNotice"

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_data: {},
      avatarSource: '',
      profile_img: '',
      isDateTimePickerVisible: false,
      isLoading: false,
      email: '',
      phone_no: '',
      first_name: '',
      last_name: '',
      dob: '',
    };
  }

  navigate_to = (val, response) => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(() => this.props.navigation.navigate('Home'), 100);
    }
  };

  state_assign = val => {
    this.setState({
      _id: response.customer[0]._id,
      email: response.customer[0].email,
      password: response.customer[0].password,
      gender: response.customer[0].gender,
      phone: response.customer[0].phone,
      first_name: response.customer[0].first_name,
      last_name: response.customer[0].last_name,
      __v: response.customer[0].__v,
      dob: response.customer[0].dob,
      profile_img: `${response.customer.profile_img}`,
    });
  };
  componentDidMount() {
    this.setState({isLoading: true});

    api
      .apiHttpRequest('getCustProfile', 'GET', '', true)
      .then(response => {
        this.setState({isLoading: false});
        if (response.status_code == 200) {
          this.setState({
            customer_data: response.customer_proile,
            first_name: response.customer_proile.first_name,
            last_name: response.customer_proile.last_name,
            email: response.customer_proile.email,
            phone_no: response.customer_proile.phone_no,
            dob: response.customer_proile.dob,
            profile_img: response.customer_proile.profile_img,
          });
        } else {
          this.refs.toast.show(response.message);
        }
      })
      .catch(error => this.setState({isLoading: false}));
  }
  image_formData = response => {
    let data = new FormData();
    Object.keys(response).forEach(key => {
      data.append(key, response[key]);
    });
    this.setState({avatarSource: 'data:image/jpeg;base64,' + response.data});
  };

  update_user_profile = () => {
    var regex_name = /^[a-zA-Z]{1,}$/;
    var regex_email = /^[a-zA-Z]{1,}([.])?[a-zA-Z0-9]{1,}([!@#$%&_-]{1})?[a-zA-Z0-9]{1,}[@]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,3}([.]{1}[a-zA-Z]{2})?$/;
    var regex_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    var regex_phone_number = /^([|+|]{1}[|9|]{1}[|1|]{1})?[7698]{1}[0-9]{1,}$/;

    if (this.state.first_name == '') {
      this.refs.toast.show('First Name is Compulsory.');
    } else if (this.state.last_name == '') {
      this.refs.toast.show('Last Name is Compulsory.');
    } else if (this.state.email == '') {
      this.refs.toast.show('Email is Compulsory.');
    } else if (this.state.phone_no == '') {
      this.refs.toast.show('Phone Number is Compulsory.');
    } else if (this.state.first_name == '') {
      this.refs.toast.show('Please select DOB.');
    } else if (!regex_name.test(this.state.first_name)) {
      this.refs.toast.show('Please enter valid first name.');
    } else if (!regex_email.test(this.state.email)) {
      this.refs.toast.show('Please enter valid email.');
    } else if (!regex_name.test(this.state.last_name)) {
      this.refs.toast.show('Please enter valid last name.');
    } else if (!regex_phone_number.test(this.state.phone_no)) {
      this.refs.toast.show('Please enter valid phone number.');
    } else if (this.state.avatarSource == '') {
      this.refs.toast.show('Image selection is Mandatory!');
    } else {
      this.setState({isLoading: true});
      // console.log('profile', this.state.avatarSource);
      api
        .apiHttpRequest(
          'profile',
          'PUT',
          {
            email: this.state.email,
            gender: this.state.gender,
            phone_no: this.state.phone_no,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            dob: this.state.dob,
            profile_img: this.state.avatarSource,
            gender: this.state.customer_data.gender,
          },
          true,
        )
        .then(response => {
          this.setState({isLoading: false});
          if (response.status_code == 200) {
            this.navigate_to(this.state.isLoading, response);
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => this.setState({isLoading: false}));
    }
  };

  formatDate = date => {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  };

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = date => {
    var date_of_birth = `${date}`.slice(0, 15);
    if (date > new Date()) {
      this.refs.toast.show('Please enter valid DOB!');
      this.hideDateTimePicker();

    } else {
      this.hideDateTimePicker();
      this.setState({dob: this.formatDate(date_of_birth)});
    }
  };

  image_picker = () => {
    ImagePicker.showImagePicker(options, response => {
      //  console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // console.log(response.data)
        this.setState({
          avatarSource: 'data:image/jpeg;base64,' + response.data,
        });
      }
    });
  };

  render() {
    return (
      Object.keys(this.state.email != '') && (
        <SafeAreaView style={styles.outer_safeareaview}>
                        <OfflineNotice/>

          <KeyboardAvoidingView enabled>
            <Toast ref="toast" />
            <Loader loading={this.state.isLoading} />
            <StatusBar backgroundColor="#e91c1a" />

            <ScrollView>
              
              <View style={styles.inner_view}>

                
                <TouchableHighlight
                  underlayColor="#E91C1A"
                  onPress={() => {
                    this.image_picker();
                  }}>
                  <ImageBackground
                    borderTopLeftRadius={75}
                    borderTopRightRadius={75}
                    borderBottomLeftRadius={75}
                    borderBottomRightRadius={75}
                    source={
                      this.state.customer_data.profile_img != null
                        ? {
                            uri: `${parentUrl}${this.state.profile_img}`,
                          }
                        : {
                            uri:
                              'https://yt3.ggpht.com/a-/AAuE7mDaG2knmVyICW5ENEMk2tVgDPvMagd6wk0xww=s88-c-k-c0x00ffffff-no-rj-mo',
                          }
                    }
                    style={styles.display_picture}>
                    <View style={styles.watermark_view}>
                      <Text style={styles.watermark_text}>Upload</Text>
                    </View>
                  </ImageBackground>
                </TouchableHighlight>

                <View style={styles.input_field_view}>
                  <InputField
                    text_input_view_style={styles.edit_profile_view}
                    text_input_style={styles.edit_profile_view_text}
                    icon={'user-alt'}
                    onChange={first_name => {
                      this.setState({first_name});
                    }}
                    value={this.state.first_name}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                </View>

                <View style={styles.input_field_view}>
                  <InputField
                    text_input_view_style={styles.edit_profile_view}
                    text_input_style={styles.edit_profile_view_text}
                    icon={'user-alt'}
                    onChange={last_name => {
                      this.setState({last_name});
                    }}
                    value={this.state.last_name}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                </View>
                <View style={styles.input_field_view}>
                  <InputField
                    text_input_view_style={styles.edit_profile_view}
                    text_input_style={styles.edit_profile_view_text}
                    icon={'envelope-open-text'}
                    onChange={email => {
                      this.setState({email});
                    }}
                    value={this.state.email}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                </View>
                <View style={styles.input_field_view}>
                  <InputField
                    text_input_view_style={styles.edit_profile_view}
                    text_input_style={styles.edit_profile_view_text}
                    icon={'mobile-alt'}
                    onChange={phone_no => {
                      this.setState({phone_no});
                    }}
                    value={`${this.state.phone_no}`}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                </View>

                <View style={[styles.input_field_view]}>
                  <View style={styles.edit_profile_view}>
                    <FontAwesome5
                      name={'birthday-cake'}
                      size={15}
                      color={'#fff'}
                    />
                    <TouchableOpacity
                      onPress={() => {
                        this.showDateTimePicker();
                      }}
                      style={styles.dob_touchable}>
                      <Text style={styles.dob_view_text}>
                        {this.state.dob != null && this.state.dob != 'null'
                          ? this.state.dob
                          : 'Date of Birth'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <DateTimePicker
                    // mode="date"
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                  />
                </View>
                <View style={styles.button_field_view}>
                  <Button
                    styleButton={styles.submit_profile_button}
                    styleButton_text={styles.submit_profile_button_text}
                    disabled={false}
                    buttonState_color="#fff"
                    textState_color="#E91C1A"
                    buttonStateHolder={false}
                    onPress={() => {
                      this.update_user_profile();
                    }}
                    text="SUBMIT"
                  />
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </SafeAreaView>
      )
    );
  }
}
export default EditProfile;
