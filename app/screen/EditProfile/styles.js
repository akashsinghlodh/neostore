import React, {Component} from "react"
import {StyleSheet} from "react-native"
const styles =StyleSheet.create({
  watermark_view:{
    alignSelf: 'center',
    backgroundColor: '#fff',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    overflow: 'hidden',
  },
  watermark_text:{
    alignSelf: 'center',
    fontSize: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  upload_button_text:{},
  upload_button:{height:30, width:90, alignItems:"center", justifyContent:"center",},
    edit_profile_view:{
      width:"95%",
    flexDirection: 'row',
    alignItems: 'center',
    height: 55,
    borderColor: 'white',
    borderWidth:1,
    paddingLeft: 10,
    paddingHorizontal:5,
  },
  dob_touchable:{width:"95%", justifyContent:"center",height:"100%"},
  dob_view_text: {
    width:"95%",
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 15,
    paddingLeft: 6,
    fontFamily:"Gotham-Medium"
  },
  edit_profile_view_text: {
      width:"95%",
      height: 40,
      backgroundColor: '#E91C1A',
      color: 'white',
      fontSize: 15,
      paddingLeft: 6,
      fontFamily:"Gotham-Medium"
    },
    submit_profile_button: {
      width:"95%",
      height: 58,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      color: '#E91C1A',
      backgroundColor: '#fff',
    },
  
    submit_profile_button_text: {
      fontSize: 21,
      paddingTop:5,
      color: '#E91C1A',
      fontFamily:"Gotham-Medium"
    },
    outer_safeareaview:{flex: 1, backgroundColor: '#E91C1A'},
    inner_view:{flex: 1, alignItems: 'center', padding: 15},
    display_picture:{
      height: 150,
      width: 150,
      borderRadius: 75,
      marginBottom: 15,
      justifyContent:"center",
      overflow:"hidden"
    },
    input_field_view:{paddingBottom: "5%",},
    button_field_view:{paddingBottom: 15, width:"100%", alignItems:"center"}

})
export default styles;