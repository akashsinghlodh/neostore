import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  no_item_view: {height: 50, alignItems: 'center', justifyContent: 'center'},
  no_item_text: {fontSize: 20, fontFamily: 'Gotham-Bookitalic'},
  outer_safeAreaView: {flex: 1},
  outer_safeareaview: {
    flex: 1,
  },
  swipeable_container: {
    borderBottomWidth: 1,
    borderBottomColor: '#7f7f7f',
  },
  dropdown_container: {width: 50},
  delete_button_view: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
  },
  order_now_button_view: {
    marginVertical: 30,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    color: '#E91C1A',
    backgroundColor: '#fff',
    width: '95%',
    alignSelf: 'center',
  },
  touchable_icon: {
    backgroundColor: 'red',
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  order_now_button: {
    fontSize: 21,
    fontFamily: 'Gotham-Medium',
    paddingTop: 5,
  },
  lower_text: {flex: 0.4, justifyContent: 'center'},
  upper_text: {fontSize: 16, fontFamily: 'Gotham-Book'},
  text_outer_view: {flex: 0.65, paddingVertical: 10},
  image_view: {
    flex: 0.35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  upper_text_view: {flex: 0.6, justifyContent: 'center'},
  order_list: {
    height: 120,
    flexDirection: 'row',
  },
  order_image: {
    height: 90,
    width: 90,
  },
  total_text: {fontSize: 16, fontFamily: 'Gotham-Medium'},
  lower_text_inner_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  category_text: {
    paddingTop: 5,
    color: '#7f7f7f',
    fontSize: 15,
    fontFamily: 'Gotham-Book',
  },
  cost_font: {
    color: '#7f7f7f',
    marginRight: 9,
    alignSelf: 'center',
    fontFamily: 'Gotham-Book',
  },
  quantity_text: {color: '#7f7f7f'},
  total_text_view: {
    height: 100,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '8%',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#7f7f7f',
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  item: {
    padding: 10,
  },
  separator: {
    height: 0.5,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#282727df',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
    fontFamily: 'Gotham-Medium',
  },
});
export default styles;
