import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Image,
  StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-whc-toast';
import CostINR from "../../components/cost_inr"
import Button from '../../components/button';
import {ScrollView} from 'react-native-gesture-handler';
import {Dropdown} from 'react-native-material-dropdown';
import Loader from '../../components/loader';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import AlertView from '../../components/alert';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import api from '../../api';
import parentUrl from '../../components/parentUrl';
import OfflineNotice from "../../components/offlineNotice"

var total;

var product_checkout = [];
class MyCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 5,
      data_product: [],
      cart_qty: [],
      product: [],
      isLoading: true,
      delete_index: null,
      cartID: '',
      updateCart: props.navigation.getParam('updateCart', false),
      imageLoaded:false
    };
  }
  re_Render = () => {
    this.fetch_data();
  };
  navigate_to = val => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(
        () =>
          this.props.navigation.navigate('MyCart', {
            qty: this.state.product_quantity,
          }),
        500,
      );
    }
  };
  order_now = () => {
    if (this.state.data_product.length > 0) {
      this.props.navigation.navigate('AddressList', {
        re_Render: this.re_Render,
      });
    } else {
      this.refs.toast.show('Please add products to your cart.');
    }
  };

  arrayUnique = array => {
    var a = array.concat();
    a.reverse();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i]._id == a[j]._id) a.splice(j--, 1);
      }
    }

    return a;
  };

  update_qty = (qty, result, index) => {
    let cart_qty = [...this.state.cart_qty];
    let data_product = [...this.state.data_product];
    if (cart_qty[index].quantity != qty) {
      cart_qty[index].quantity = qty;
      this.setState({cart_qty: cart_qty});
      AsyncStorage.setItem('cart', JSON.stringify(this.state.cart_qty));
    }
  };
  onSuccess = () => {
    const {delete_index} = this.state;
    let cart_qty = [...this.state.cart_qty];
    let data_product = [...this.state.data_product];

    api
      .apiHttpRequest(
        `deleteCustomerCart/${cart_qty[delete_index].product_id}`,
        'DELETE',
        {},
        true,
      )
      .then(response => {
        this.setState({isLoading: false});
        if (!this.state.isLoading) {
          if (delete_index != null) {
            cart_qty.splice(delete_index, 1);
            data_product.splice(delete_index, 1);
          }
          this.setState({data_product: data_product});
          this.setState({cart_qty: cart_qty});

          AsyncStorage.setItem('cart_data', JSON.stringify(data_product));
          AsyncStorage.setItem('cart', JSON.stringify(cart_qty));

          this.fetch_data();
        }
      })
      .catch(error => console.log(error));
  };
  delete_cart_item = result => {
  };
  swipe_delete_button = (result, index) => {
    return (
      <View style={styles.delete_button_view}>
        <TouchableOpacity
          onPress={() => {
            this.setState({delete_index: index});

            this.setState({cartID: result._id}),
              AlertView(
                'Alert',
                'Are you sure you want to delete ?',
                this.onSuccess,
              );
          }}
          style={styles.touchable_icon}>
          <MaterialIcons name="delete" size={25} color={'#fff'} />
        </TouchableOpacity>
      </View>
    );
  };

  fetch_data = async () => {
    this.setState({isLoading: true});
    cart = await AsyncStorage.getItem('cart');
    cart = JSON.parse(cart);
    cart = cart == null ? [] : cart;
    cart = this.arrayUnique(cart);
    cart_details = await AsyncStorage.getItem('cart_data');
    cart_details = JSON.parse(cart_details);
    cart_details = cart_details == null ? [] : cart_details;
    cart_details = this.arrayUnique(cart_details);

    var cart_api = [];
    var cart_id_api = [];
    api
      .apiHttpRequest('getCartData', 'GET', {}, true)
      .then(response => {
        this.setState({isLoading: false});
        if (response.status_code == 200) {
          if (response.product_details != undefined) {

            response.product_details.map((result, index) => {
              var cart_id = {
                _id: result.product_id._id,
                product_id: result.product_id._id,
                quantity: result.quantity,
              };
              cart_api.push(result.product_id);
              cart_id_api.push(cart_id);
            });
          }
        }

        var cart_data = this.arrayUnique(cart_api.concat(cart_details));
        var cart_qty = this.arrayUnique(cart_id_api.concat(cart));
        AsyncStorage.setItem('cart', JSON.stringify(cart_qty));
        AsyncStorage.setItem('cart_data', JSON.stringify(cart_data));
        AsyncStorage.setItem('cart_count', `${cart_qty.length}`);

        this.setState({data_product: cart_data});
        this.setState({cart_qty: cart_qty});

      })
      .catch(error => {
        this.setState({isLoading: false});
      });
  };

  componentDidMount() {
    this.fetch_data();
  }

  assign(product) {
    product_checkout = product;
  }
  render_orders = () => {
    var product = [];
    total = 0;
    const {data_product} = this.state;
    const {cart_qty} = this.state;

    if (data_product.length > 0 && cart_qty.length > 0) {
      return (
        Object.keys(data_product).length > 0 &&
        data_product.map((result, ind) => {
          let category = [];
          let data = [];
          const quantity_max= result.product_stock<10?result.product_stock:10;
          for (let i = 0; i < result.product_stock; i++) {
            data[i] = i + 1;
          }

          data.slice(0, quantity_max).map(
            (result, index) =>
              (category[index] = {
                value: result,
              }),
          );
          product[ind] = {
            product_id: result.product_id.product_id,
            quantity: result.quantity,
          };
          cost_product = result.product_cost * cart_qty[ind].quantity;
          total = total + result.product_cost * cart_qty[ind].quantity;
          return (
            <Swipeable
              containerStyle={styles.swipeable_container}
              renderRightActions={() => this.swipe_delete_button(result, ind)}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ProductDetails', {
                    product_id: `${result.product_id}`,
                    title: `${result.product_name}`,
                  });
                }}
                style={styles.order_list}>
                <View style={styles.image_view}>
                  <Image
                  onLoad={()=>{this.setState({imageLoaded:true})}}
                    resizeMode="contain"
                    style={styles.order_image}
                    source={this.state.imageLoaded?{
                      uri: `${parentUrl}${result.product_image}`,
                    }:require("../../assets/images.jpeg")}
                  />
                </View>

                <View style={styles.text_outer_view}>
                  <View style={styles.upper_text_view}>
                    <Text style={styles.upper_text}>{result.product_name}</Text>
                    <Text style={styles.category_text}>
                      ({result.category_id.category_name})
                    </Text>
                  </View>
                  <View style={styles.lower_text}>
                    <View style={styles.lower_text_inner_view}>
                      <Dropdown
                        value={cart_qty[ind].quantity}
                        containerStyle={styles.dropdown_container}
                        data={category}
                        onChangeText={value => {
                          this.update_qty(value, result, ind);
                        }}></Dropdown>
                      <Text style={styles.cost_font}>
                        <FontAwesome name="rupee" size={15} color="#7f7f7f" />
                        &nbsp; {CostINR(cost_product)}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </Swipeable>
          );
        }, this.assign(product))
      );
    } else {
      return (
        <View style={styles.no_item_view}>
          <Text style={styles.no_item_text}>Your Cart Is Empty.</Text>
        </View>
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.outer_safeareaview}>
        <OfflineNotice/>
        <Toast ref="toast" />
        <Loader loading={this.state.isLoading} />
        <StatusBar backgroundColor="#e91c1a" />

        <ScrollView>
          {this.render_orders()}
          <View style={styles.total_text_view}>
            <Text style={styles.total_text}>Total</Text>
            <Text style={styles.total_text}>
              <FontAwesome name="rupee" size={20} color="#333333" />
              &nbsp;{CostINR(total)}
            </Text>
          </View>
          <Button
            styleButton={styles.order_now_button_view}
            styleButton_text={styles.order_now_button}
            disabled={false}
            buttonState_color="#E91C1A"
            textState_color="#fff"
            buttonStateHolder={false}
            onPress={() => this.order_now()}
            text="ORDER NOW"
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default MyCart;
