import React, {Component} from 'react';
import {View, Text, ScrollView, SafeAreaView, StatusBar} from 'react-native';
import styles from './styles';
import api from '../../api';
import InputField from '../../components/inputfield';
import Button from '../../components/button';
import Loader from '../../components/loader';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-whc-toast';
import OfflineNotice from "../../components/offlineNotice"

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isLoading: false,
    };
  }
  navigate_to = (val, response) => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(
        () => this.props.navigation.navigate('ForgetPasswordOTP'),
        500,
      );
    }
  };

  validate() {
    if (this.state.email == '') {
      this.refs.toast.show('Please enter your Username!');
    } else {
      this.setState({isLoading: true});

      api
        .apiHttpRequest('forgotPassword', 'POST', {
          email: this.state.email.toLowerCase(),
        })
        .then(response => {
          this.setState({isLoading: false});
          // console.log(response)
          if (response.status_code == 200) {
            AsyncStorage.setItem('token', response.token);

            this.navigate_to(this.state.isLoading, response);
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          this.refs.toast.show(error.message);
        });
    }
  }
  render() {
    return (
      <SafeAreaView style={styles.outer_flex}>
<OfflineNotice/>
        <Toast ref="toast" />

        <ScrollView>
          <View style={styles.outer_flex}>
            <Loader loading={this.state.isLoading} />
            <StatusBar backgroundColor="#e91c1a" />

            <View style={styles.inner_flex_end}>
              <Text style={styles.inner_flex_end_text}>NeoSTORE</Text>

              <View style={styles.view_input}>
                <InputField
                  text_input_view_style={styles.forget_password_input_view}
                  text_input_style={styles.forget_password_input}
                  placeholder={'Username'}
                  icon={'user-alt'}
                  onChange={email => {
                    this.setState({email});
                  }}
                  value={this.state.password}
                  keyboardType="default"
                  returnKeyType={'next'}
                  secureTextEntry={false}
                  blurOnSubmit={false}
                />
              </View>
              <Button
                buttonState_color={'#fff'}
                styleButton={styles.forget_password_button}
                styleButton_text={styles.forget_password_button_text}
                disabled={false}
                buttonStateHolder={false}
                onPress={() => this.validate()}
                text="SUBMIT"
                textState_color={'#E91C1A'}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default ForgetPassword;
