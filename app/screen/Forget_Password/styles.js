import React, {Component} from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  view_input: {paddingBottom: '5%'},
  forget_password_button: {
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#fff',
    width: '95%',
  },

  forget_password_button_text: {
    fontSize: 21,
    color: '#E91C1A',
  },
  forget_password_input_view: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 58,
    borderColor: 'white',
    borderWidth: 1,
    paddingLeft: 10,
  },

  forget_password_input: {
    width: '90%',
    height: 47,
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 16,
    paddingLeft: '5%',
    fontFamily: 'Gotham-Medium',
  },

  outer_flex: {
    flex: 1,
    backgroundColor: '#E91C1A',
    paddingHorizontal: '3%',
  },
  inner_flex_middle: {
    backfaceVisibility: 'visible',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inner_middle_text: {
    color: '#fff',
    fontSize: 24,
  },

  inner_flex_end: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: '35%',
  },

  inner_flex_end_text: {
    marginBottom: '7%',
    color: '#fff',
    fontSize: 35,
    fontFamily: 'Gotham-Bold',
  },

  user_id_field: {
    paddingLeft: '4%',
    marginTop: '10%',
    height: 55,
    borderColor: '#fff',
    borderWidth: 1,
    width: '95%',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#fff',
  },

  submit_button: {
    marginTop: '7%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#fff',
  },

  submit_button_text: {
    fontSize: 30,
    color: '#E91C1A',
  },
});
export default styles;
