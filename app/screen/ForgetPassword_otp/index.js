import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StatusBar} from 'react-native';
import styles from './styles';
import InputField from '../../components/inputfield';
import api from '../../api';
import Loader from '../../components/loader';
import Toast from 'react-native-whc-toast';
import OfflineNotice from "../../components/offlineNotice"

class ForgetPasswordOTP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      new_password: '',
      confirm_password: '',
      isLoading: false,
    };
  }
  navigate_to = (val, response) => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(() => this.props.navigation.navigate('Login'), 500);
    }
  };

  validate() {
    if (this.state.otp == '') {
      this.refs.toast.show('Please enter OTP!');
    } else if (this.state.new_password == '') {
      this.refs.toast.show('Please enter your New Password!');
    } else if (this.state.confirm_password == '') {
      this.refs.toast.show('Please Confirm your Password!');
    } else {
      this.setState({isLoading: true});

      api
        .apiHttpRequest(
          'recoverPassword',
          'POST',
          {
            otpCode: this.state.otp,
            newPass: this.state.new_password,
            confirmPass: this.state.confirm_password,
          },
          true,
        )
        .then(response => {
          // console.log(response)
          this.setState({isLoading: false});

          this.navigate_to(this.state.isLoading, response);
        })
        .catch(error => {
          this.setState({isLoading: false});

          this.refs.toast.show(error.message);
        });
    }
  }

  render() {
    return (
      <View style={styles.outer_flex}>
        <OfflineNotice/>
        <Toast ref="toast" />
        <View style={styles.inner_flex_end}>
          <Text style={styles.inner_flex_end_text}>NeoSTORE</Text>
          <Loader loading={this.state.isLoading} />
          <StatusBar backgroundColor="#e91c1a" />
          <View style={styles.input_field_view}>
            <InputField
              text_input_view_style={styles.edit_profile_view}
              text_input_style={styles.edit_profile_view_text}
              icon={'lock'}
              onChange={otp => {
                this.setState({otp});
              }}
              placeholder={'OTP'}
              keyboardType="default"
              returnKeyType={'next'}
              secureTextEntry={true}
              blurOnSubmit={false}
            />
          </View>
          <View style={styles.input_field_view}>
            <InputField
              text_input_view_style={styles.edit_profile_view}
              text_input_style={styles.edit_profile_view_text}
              icon={'lock'}
              onChange={new_password => {
                this.setState({new_password});
              }}
              placeholder={'NEW PASSWORD'}
              keyboardType="default"
              returnKeyType={'next'}
              secureTextEntry={true}
              blurOnSubmit={false}
            />
          </View>
          <View style={styles.input_field_view}>
            <InputField
              text_input_view_style={styles.edit_profile_view}
              text_input_style={styles.edit_profile_view_text}
              icon={'lock'}
              onChange={confirm_password => {
                this.setState({confirm_password});
              }}
              placeholder={'CONFIRM PASSWORD'}
              keyboardType="default"
              returnKeyType={'next'}
              secureTextEntry={true}
              blurOnSubmit={false}
            />
          </View>
          <TouchableOpacity
            style={styles.submit_button}
            onPress={() => {
              this.validate();
            }}>
            <Text style={styles.submit_button_text}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default ForgetPasswordOTP;
