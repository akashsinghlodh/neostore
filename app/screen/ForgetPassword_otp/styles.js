import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  edit_profile_view:{
    flexDirection: 'row',
    alignItems: 'center',
    height: 58,
    borderColor: 'white',
    borderWidth:1,
    paddingLeft: 10,
  },
  
  edit_profile_view_text: {
      width:"92%",
      height: 47,
      backgroundColor: '#E91C1A',
      color: 'white',
      fontSize: 16,
      paddingLeft: "5%"
  
    },
    submit_profile_button: {
      width:"100%",
      height: 58,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      color: '#E91C1A',
      backgroundColor: '#fff',
    },
  
    submit_profile_button_text: {
      fontSize: 21,
      color: '#E91C1A',
    },
    outer_safeareaview:{flex: 1, backgroundColor: '#E91C1A'},
    inner_view:{flex: 1, alignItems: 'center', padding: 15},
    display_picture:{
      height: 200,
      width: 200,
      borderRadius: 100,
      marginBottom: 15,
    },
    input_field_view:{paddingBottom: "5%"},

  outer_flex: {
    flex: 1,
    backgroundColor: "#E91C1A",
    paddingHorizontal: "7%"
  },
  inner_flex_middle: {
    backfaceVisibility: "visible",
    justifyContent: "center",
    alignItems: "center"
  },
  inner_middle_text: {
    color: "#fff",
    fontSize: 24
  },

  inner_flex_end: {
      paddingTop:"15%",
    alignItems: "center",
    marginBottom:15
  },

  inner_flex_end_text: {
    color: "#fff",
    fontSize: 35,
    fontWeight: "bold",
    marginBottom:15
  },

  user_id_field: {
    padding: "4%",
    marginTop: "6%",
    height: "11%",
    borderColor: "#fff",
    borderWidth: 2,
    width: "100%",
    fontSize: 16,
    fontWeight: "bold",
    color: "#fff"
  },

  submit_button: {
    height: 50,
    alignItems: "center",
    width: "100%",
    backgroundColor: "#fff",
    justifyContent:"center"
  },

  submit_button_text: {
    fontSize: 21,
    color: "#E91C1A"
  }
});
export default styles;
