import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ScrollView,
} from 'react-native';
import styles from './styles';
import InputField from '../../components/inputfield';
import api from '../../api';
import Loader from '../../components/loader';
import Toast from 'react-native-whc-toast';
import OfflineNotice from "../../components/offlineNotice"

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
      new_password: '',
      confirm_password: '',
      isLoading: false,
    };
  }
  validate() {
    if (this.state.old_password == '') {
      this.refs.toast.show('Please enter your Old Password!');
    } else if (this.state.new_password == '') {
      this.refs.toast.show('Please enter your New Password!');
    } else if (this.state.confirm_password == '') {
      this.refs.toast.show('Please Confirm your Password!');
    } else {
      this.setState({isLoading: true});

      api
        .apiHttpRequest(
          'changePassword',
          'POST',
          {
            oldPass: this.state.old_password,
            newPass: this.state.new_password,
            confirmPass: this.state.confirm_password,
          },
          true,
        )
        .then(response => {
          this.setState({isLoading: false});
          this.refs.toast.show(response.message);
          setTimeout(() => this.props.navigation.navigate('Home'), 100);
        })
        .catch(error => {
          this.setState({isLoading: false});
          this.refs.toast.show(error.message);
        });
    }
  }

  render() {
    return (
      <View style={styles.outer_flex}>
        <OfflineNotice/>
        <Toast ref="toast" />
        <ScrollView>
          <View style={styles.inner_flex_end}>
            <StatusBar backgroundColor="#e91c1a" />
            <Loader loading={this.state.isLoading} />
            <Text style={styles.inner_flex_end_text}>NeoSTORE</Text>

            <View style={styles.input_field_view}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'lock'}
                onChange={old_password => {
                  this.setState({old_password});
                }}
                placeholder={'OLD PASSWORD'}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={true}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.input_field_view}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'lock'}
                onChange={new_password => {
                  this.setState({new_password});
                }}
                placeholder={'NEW PASSWORD'}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={true}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.input_field_view}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'lock'}
                onChange={confirm_password => {
                  this.setState({confirm_password});
                }}
                placeholder={'CONFIRM PASSWORD'}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={true}
                blurOnSubmit={false}
              />
            </View>

            <TouchableOpacity
              style={styles.submit_button}
              onPress={() => {
                this.validate();
              }}>
              <Text style={styles.submit_button_text}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default ResetPassword;
