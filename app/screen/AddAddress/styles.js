import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  flex_row: {flexDirection: 'row'},
  background_scrollview: {backgroundColor: '#ededed'},
  background_safeareaview: {
    flex: 1,
    backgroundColor: '#ededed',
    marginHorizontal: '5%',
    marginVertical: '5%',
  },
  outer_view: {backgroundColor: '#ededed', flex: 1},
  text_label: {
    color: '#333333',
    fontSize: 18,
    paddingBottom: 10,
    fontFamily: 'Gotham-Medium',
  },
  text_inputfield: {
    fontSize: 15,
    color: '#E91B1A',
    paddingTop: 7,
    paddingBottom: 7,
  },
  save_address_button: {
    backgroundColor: '#E91B1A',
    alignItems: 'center',
    justifyContent: 'center',
    height: 75,
    borderRadius: 10,
  },
  save_address_button_text: {
    fontSize: 30,
    color: '#fff',
    paddingTop: 7,
    fontFamily: 'Gotham-Medium',
  },
  address_input_view: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  address_input_field: {
    width: '100%',
    height: 90,
    backgroundColor: '#fff',
    fontSize: 20,
    padding: 5,
    color: '#333',
    fontFamily: 'Gotham-Book',
  },
  horizontal_inputfields: {width: '50%', paddingRight: '2%'},
  input_field: {
    backgroundColor: '#fff',
    height: 50,
    fontSize: 20,
    width: '100%',
  },
});
export default styles;
