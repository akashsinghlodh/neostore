import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import OfflineNotice from "../../components/offlineNotice"

import Loader from '../../components/loader';
import styles from './styles';
import api from '../../api';
import Button from '../../components/button';
import InputField from '../../components/inputfield';
class AddAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: '',
      city: '',
      city_landmark: '',
      state_address: '',
      zip_code: '',
      country: '',
      address_message: '',
      city_message: '',
      state_message: '',
      zip_code_message: '',
      country_message: '',
      isLoading: false,
    };
  }
  navigate_to = (val, response) => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(
        () =>
          this.props.navigation.navigate('AddressList', {updateList: 'update'}),
        500,
      );
    }
  };
  saving_address = () => {
    var regex_address = /^[a-zA-z0-9, .]{5,50}/;
    var regex_text_address = /^[a-zA-Z]{3,}/;
    var regex_zip_code = /^[4]{1}[0-9]{5}/;
    if (this.state.address == '') {
      this.setState({address_message: 'PLEASE ENTER THE ADDRESS!'});
    } else if (!regex_address.test(this.state.address)) {
      this.setState({address_message: 'INVALID ADDRESS'});
    } else if (this.state.city == '') {
      this.setState({
        address_message: '',
        city_message: 'PLEASE ENTER THE CITY!',
      });
    } else if (!regex_text_address.test(this.state.city)) {
      this.setState({address_message: '', city_message: 'INVALID CITY NAME!'});
    } else if (this.state.city_landmark == '') {
      this.setState({
        city_message: '',
        city_landmark_message: 'PLEASE ENTER THE CITY!',
      });
    } else if (!regex_text_address.test(this.state.city_landmark)) {
      this.setState({
        city_message: '',
        city_landmark_message: 'INVALID CITY NAME!',
      });
    } else if (this.state.state_address == '') {
      this.setState({
        city_landmark_message: '',
        state_message: 'PLEASE ENTER THE STATE!',
      });
    } else if (!regex_text_address.test(this.state.state_address)) {
      this.setState({
        city_landmark_message: '',
        state_message: 'INVALID STATE NAME!',
      });
    } else if (this.state.zip_code == '') {
      this.setState({
        state_message: '',
        zip_code_message: 'PLEASE ENTER THE ZIP CODE!',
      });
    } else if (!regex_zip_code.test(this.state.zip_code)) {
      this.setState({state: '', zip_code_message: 'INVALID ZIP CODE!'});
    } else if (this.state.country == '') {
      this.setState({
        zip_code_message: '',
        country_message: 'PLEASE ENTER THE COUNTRY!',
      });
    } else if (!regex_text_address.test(this.state.country)) {
      this.setState({
        zip_code_message: '',
        country_message: 'INVALID COUNTRY NAME!',
      });
    } else {
      this.setState({
        address_message: '',
        city_message: '',
        city_landmark_message: '',
        state_message: '',
        zip_code_message: '',
        country_message: '',
      });
      this.setState({isLoading: true});
      
      api
        .apiHttpRequest(
          'address',
          'POST',
          {
            address: this.state.address,
            pincode: this.state.zip_code,
            city: this.state.city,
            state: this.state.state_address,
            country: this.state.country,
          },
          true,
        )
        .then(response => {
          // console.log(response);
          this.setState({isLoading: false});

          if (response.status_code == 200) {
            this.navigate_to(this.state.isLoading, response);
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          this.refs.toast.show(error.message);
          this.setState({isLoading: false});
        });
    }
  };

  render() {
    return (
      <View style={styles.outer_view}>
        <OfflineNotice/>
        <Toast ref="toast" />

        <SafeAreaView style={styles.background_safeareaview}>
          <KeyboardAvoidingView enabled>
            <ScrollView bounces={true}>
              <Loader loading={this.state.isLoading} />
              <StatusBar backgroundColor="#e91c1a" />
              <View>
                <Text style={styles.text_label}>ADDRESS</Text>

                <InputField
                  text_input_view_style={styles.address_input_view}
                  text_input_style={styles.address_input_field}
                  onChange={address => {
                    this.setState({address});
                  }}
                  multiline={true}
                  value={this.state.address}
                  keyboardType="default"
                  returnKeyType={'next'}
                  secureTextEntry={false}
                  blurOnSubmit={false}
                />

                <Text style={styles.text_inputfield}>
                  {this.state.address_message}
                </Text>
                <Text style={styles.text_label}>CITY</Text>
                <InputField
                  text_input_view_style={styles.address_input_view}
                  text_input_style={styles.input_field}
                  onChange={city => {
                    this.setState({city});
                  }}
                  numberOfLines={1}
                  multiline={false}
                  value={this.state.city}
                  keyboardType="default"
                  returnKeyType={'next'}
                  secureTextEntry={false}
                  blurOnSubmit={false}
                />
                <Text style={styles.text_inputfield}>
                  {this.state.city_message}
                </Text>
              </View>

              <View style={styles.flex_row}>
                <View style={styles.horizontal_inputfields}>
                  <Text style={styles.text_label}>CITY</Text>
                </View>
                <View style={styles.horizontal_inputfields}>
                  <Text style={styles.text_label}>STATE</Text>
                </View>
              </View>

              <View style={styles.flex_row}>
                <View style={styles.horizontal_inputfields}>
                  <InputField
                    text_input_view_style={styles.address_input_view}
                    text_input_style={styles.input_field}
                    onChange={city_landmark => {
                      this.setState({city_landmark});
                    }}
                    multiline={false}
                    value={this.state.city_landmark}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />

                  <Text style={styles.text_inputfield}>
                    {this.state.city_landmark_message}
                  </Text>
                </View>
                <View style={styles.horizontal_inputfields}>
                  <InputField
                    text_input_view_style={styles.address_input_view}
                    text_input_style={styles.input_field}
                    onChange={state_address => {
                      this.setState({state_address});
                    }}
                    multiline={false}
                    value={this.state.state_address}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                  <Text style={styles.text_inputfield}>
                    {this.state.state_message}
                  </Text>
                </View>
              </View>

              <View style={styles.flex_row}>
                <View style={styles.horizontal_inputfields}>
                  <Text style={styles.text_label}>ZIP CODE</Text>
                </View>
                <View style={styles.horizontal_inputfields}>
                  <Text style={styles.text_label}>COUNTRY</Text>
                </View>
              </View>

              <View style={styles.flex_row}>
                <View style={styles.horizontal_inputfields}>
                  <InputField
                    text_input_view_style={styles.address_input_view}
                    text_input_style={styles.input_field}
                    onChange={zip_code => {
                      this.setState({zip_code});
                    }}
                    multiline={false}
                    value={this.state.zip_code}
                    keyboardType="numeric"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                  <Text style={styles.text_inputfield}>
                    {this.state.zip_code_message}
                  </Text>
                </View>
                <View style={styles.horizontal_inputfields}>
                  <InputField
                    text_input_view_style={styles.address_input_view}
                    text_input_style={styles.input_field}
                    onChange={country => {
                      this.setState({country});
                    }}
                    multiline={false}
                    value={this.state.country}
                    keyboardType="default"
                    returnKeyType={'next'}
                    secureTextEntry={false}
                    blurOnSubmit={false}
                  />
                  <Text style={styles.text_inputfield}>
                    {this.state.country_message}
                  </Text>
                </View>
              </View>
              <View style={{paddingVertical: 20}}>
                <Button
                  styleButton={styles.save_address_button}
                  styleButton_text={styles.save_address_button_text}
                  disabled={false}
                  buttonState_color={'#e91b1a'}
                  textState_color={'#fff'}
                  onPress={() => this.saving_address()}
                  text="SAVE ADDRESS"
                />
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
    );
  }
}

export default AddAddress;
