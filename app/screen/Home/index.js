import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import styles from './styles';
import api from '../../api';
import Icon from 'react-native-vector-icons/FontAwesome5';
import parentUrl from '../../components/parentUrl';
import ImageSlider from 'react-native-image-slider';
import OfflineNotice from "../../components/offlineNotice"

const {width} = Dimensions.get('window');
const icon_size = (width - 10) / 6;

const data = [
  {
    text_position: 'bottom',
    title: 'Chairs',
    icon: 'chair',
    bgColor: '#E91C1A',
    titlePosition: 'Right',
    iconPosition: 'Left',
    _id: '5cfe3c6fea821930af692820',
  },
  {
    text_position: 'top',
    title: 'Sofa',
    icon: 'couch',
    bgColor: '#E91C1A',
    titlePosition: 'Left',
    iconPosition: 'Right',
    _id: '5cfe3c5aea821930af69281e',
  },
  {
    text_position: 'top',
    title: 'Table',
    icon: 'table',
    bgColor: '#E91C1A',
    titlePosition: 'Right',
    iconPosition: 'Left',
    _id: '5cfe3c79ea821930af692821',
  },
  {
    text_position: 'bottom',
    title: 'Almirah',
    icon: 'inbox',
    bgColor: '#E91C1A',
    titlePosition: 'Left',
    iconPosition: 'Right',
    _id: '5d14c15101ae103e6e94fbe0',
  },

  {
    text_position: 'top',
    title: 'Bed',
    icon: 'bed',
    bgColor: '#E91C1A',
    titlePosition: 'Left',
    iconPosition: 'Right',
    _id: '5cfe3c65ea821930af69281f',
  },
];
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_data: {},
      entries: [],
      images: [],
      slider_navigate_screen: [],
      isLoading: false,
      return_to_ind: 0,
      cart_data: [],
      cart_id: [],
    };
  }

  componentDidMount() {
    this.setState({isLoading: true});
    // api.flag=true;

    api
      .apiHttpRequest('getAllCategories', 'GET', '')
      .then(response => {
        this.setState({isLoading: false});

        if (response.status_code == 200) {
          const imagesData = [...this.state.images];
          const slider_navigate_screen_data = [
            ...this.state.slider_navigate_screen,
          ];
          this.setState({entries: response.category_details});
          response.category_details.length > 0 &&
            response.category_details.map((data, index) => {
              imagesData[index] = `${parentUrl}${data.product_image}`;
              slider_navigate_screen_data[index] = data;
            });
          this.setState({
            images: imagesData,
            slider_navigate_screen: slider_navigate_screen_data,
          });
        }
      })
      .catch(error => error);
  }
  card_arrangement(result) {
    if (result.text_position == 'top') {
      return (
        <View style={styles.renderItems_block_outer}>
          <View
            style={[
              styles.renderItems_block_text,
              {
                justifyContent:
                  result.titlePosition == 'Right' ? 'flex-end' : 'flex-start',
              },
            ]}>
            <Text style={[styles.renderItems_icons]}>{result.title}</Text>
          </View>
          <View
            style={[
              styles.renderItems_block_icon,
              {
                justifyContent:
                  result.iconPosition == 'Right' ? 'flex-end' : 'flex-start',
              },
            ]}>
            <Text style={styles.card_icon}>
              <Icon name={result.icon} color="#fff" size={icon_size} />
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.renderItems_block_outer}>
          <View
            style={[
              styles.renderItems_block_icon,
              {
                justifyContent:
                  result.iconPosition == 'Right' ? 'flex-end' : 'flex-start',
              },
            ]}>
            <Text style={styles.card_icon_top}>
              <Icon name={result.icon} color="#fff" size={icon_size} />
            </Text>
          </View>
          <View
            style={[
              styles.renderItems_block_text,
              {
                justifyContent:
                  result.titlePosition == 'Right' ? 'flex-end' : 'flex-start',
              },
            ]}>
            <Text style={[styles.renderItems_icons]}>{result.title}</Text>
          </View>
        </View>
      );
    }
  }
  renderItems = () => {
    return data.map(result => {
      return (
        <TouchableOpacity
          key={result._id}
          onPress={() => {
            this.props.navigation.navigate('ProductList', {
              _id: result._id,
              title: result.title,
            });
          }}
          style={styles.renderItems_style}>
          {this.card_arrangement(result)}
        </TouchableOpacity>
      );
    });
  };

  render() {
    const {entries, images} = this.state;
    var return_to_ind = false;
    return (
      entries.length > 0 &&
      images.length > 0 && (
        <SafeAreaView style={styles.main_view}>
          <OfflineNotice/>
          <StatusBar backgroundColor="#e91c1a" />
          <ScrollView>
            <SafeAreaView style={styles.upper_view}>
              <ImageSlider
                style={styles.image_slider}
                loopBothSides
                autoPlayWithInterval={2000}
                images={this.state.images}
                customButtons={(position, move) => {
                  // console.log('changed', position);
                  if (position == ((this.state.images).length-1)) {
                   setTimeout(()=> move(0),2000)
                  }
                  return (
                    <View style={styles.pagination_slider}>
                      {images.map((image, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            onPress={() => {
                              move(index);
                            }}
                            style={
                              position == index
                                ? styles.active_dot_slider
                                : styles.inactive_dot_slider
                            }></TouchableOpacity>
                        );
                      })}
                    </View>
                  );
                }}
                onPress={index => {
                  this.state.slider_navigate_screen[index.index] != undefined
                    ? this.props.navigation.navigate('ProductList', {
                        _id: `${
                          this.state.slider_navigate_screen[index.index]._id
                        }`,
                        title: `${
                          this.state.slider_navigate_screen[index.index]
                            .category_name
                        }`,
                      })
                    : null;
                }}
              />
            </SafeAreaView>
            <View style={styles.bottom_view}>{this.renderItems()}</View>
          </ScrollView>
        </SafeAreaView>
      )
    );
  }
}
export default Home;
