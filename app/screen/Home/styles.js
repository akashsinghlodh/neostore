import React, {Component} from 'react';
import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('window');
const width_block = (width - 30) / 2;
const height_imageslider = width * 0.6;
const styles = StyleSheet.create({
  main_view: {flex: 1},
  pagination_slider:{justifyContent:"center",flexDirection:"row", },
  active_dot_slider:{backgroundColor:"#333", marginHorizontal:3,width:12,height:12,borderRadius:6, marginTop:-20,borderColor:"#fff",borderWidth:1},
  inactive_dot_slider:{backgroundColor:"#e91b1a", marginHorizontal:3,width:11,height:11,borderRadius:6, marginTop:-20,borderColor:"#fff",borderWidth:1},
  upper_view: {flex: 0.4, marginBottom: 10},
  card_icon: {alignSelf: 'flex-end'},
  card_icon_top: {alignSelf: 'flex-end', alignSelf: 'flex-start'},
  image_slider: {height: height_imageslider},
  renderItems_style: {
    height: width_block,
    width: width_block,
    marginBottom: 10,
    marginRight:10,
    backgroundColor: '#E91C1A',
  },
  renderItems_block_outer: {flex: 1},
  renderItems_block_text: {flexDirection: 'row', flex: 0.3, padding: '5%'},
  renderItems_block_icon: {flexDirection: 'row', flex: 0.7, padding: '5%'},
  renderItems_icons: {
    fontSize: 25,
    color: '#fff',
    flexDirection: 'row',
    fontFamily: 'Gotham-Medium',
  },
  bottom_view: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // justifyContent:"center",
    paddingLeft:10,
    alignItems:"center",
  },
});
export default styles;
