import {StyleSheet} from "react-native"
const styles =StyleSheet.create({
    touchable_location_list: {
        height: 100,
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#bfbfbf',
        flexDirection: 'row',
      },
      view_icon:{flex: 0.2, alignItems: 'center', justifyContent: 'center'},
     view_text: {flex: 0.8, justifyContent: 'center'},
     upper_view:{flex: 0.4, justifyContent: 'center'},
     upper_text:{fontFamily:"Gotham-Bold", fontSize: 16, color: '#333333'},
     lower_view:{flex: 0.6},
     lower_text:{color: '#4f4f4f', fontFamily:"Gotham-Book"},
     outer_safeareaview:{flex: 1},
     map_view:{height: 250}
})
export default styles;