import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {Marker} from 'react-native-maps';
import Entypo from 'react-native-vector-icons/Entypo';
import styles from './styles';
import OfflineNotice from "../../components/offlineNotice"

class StoreLocator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: '',
      longitude: '',
      longitudeDelta: '',
      latitudeDelta: '',
      markers: [
        {
          latlng: {
            latitude: 19.008451,
            longitude: 72.834081,
            latitudeDelta: 0.0015,
            longitudeDelta: 0.0004121,
          },
          title: 'NeoSOFT Technologies',
          description:
            '4th Floor, The Ruby, 29, Tulsi Pipe Rd, Dadar West, Mumbai, Maharashtra 400028',
        },
        {
          latlng: {
            latitude: 19.01302,
            longitude: 72.84465,
            latitudeDelta: 0.0015,
            longitudeDelta: 0.0004121,
          },
          title: 'NeoSOFT Technologies',
          description:
            'Unique Industrial Estate, 124, SVS Rd, Off, Prabhadevi, Mumbai, Maharashtra 400025',
        },
        {
          latlng: {
            latitude: 19.0334,
            longitude: 73.019,
            latitudeDelta: 0.0015,
            longitudeDelta: 0.0004121,
          },
          title: 'NeoSOFT Technologies',
          description:
            'Unit No 501, Sigma IT Park, Plot No R-203,204, Midc TTC Industrial Area. Rabale, Navi Mumbai, Maharashtra 400701',
        },
      ],
    };
  }
  render_locations = () => {
    const {markers} = this.state;

    return markers.map(result => {
      return (
        <TouchableOpacity
          onPress={() => {
            this.setState({
              latitude: result.latlng.latitude,
              longitude: result.latlng.longitude,
              latitudeDelta: result.latlng.latitudeDelta,
              longitudeDelta: result.latlng.longitudeDelta,
            });
          }}
          style={styles.touchable_location_list}>
          <View style={styles.view_icon}>
            <Entypo name="location" size={30} color={'#4f4f4f'} />
          </View>

          <View style={styles.view_text}>
            <View style={styles.upper_view}>
              <Text style={styles.upper_text}>{result.title}</Text>
            </View>
            <View style={styles.lower_view}>
              <Text style={styles.lower_text}>{result.description}</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
  };
  componentDidMount() {
    this.setState({
      latitude: this.state.markers[0].latlng.latitude,
      longitude: this.state.markers[0].latlng.longitude,
      latitudeDelta: 0.4,
      longitudeDelta: 0.315,
    });
  }
  render_markers = () => {
    return this.state.markers.map(marker => {
      return (
        <Marker
          coordinate={marker.latlng}
          title={marker.title}
          description={marker.description}
        />
      );
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.outer_safeareaview}>
        <OfflineNotice/>
        <StatusBar backgroundColor="#e91c1a" />
        <ScrollView>
        <MapView
          style={styles.map_view}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}>
          {this.render_markers()}
        </MapView>

        {this.render_locations()}</ScrollView>
      </SafeAreaView>
    );
  }
}

export default StoreLocator;
