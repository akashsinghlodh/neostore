import React, {Component} from 'react';
import {
  Text,
  View,
  Linking,
  StatusBar,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import Button from '../../components/button';
import Toast from 'react-native-whc-toast';
import Checkbox from 'react-native-modest-checkbox';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import api from '../../api';
import OfflineNotice from "../../components/offlineNotice"

import styles from './styles';
import InputField from '../../components/inputfield';
import Loader from '../../components/loader';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ParentUrl from "../../components/parentUrl"
import parentUrl from '../../components/parentUrl';
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirm_password: '',
      phone_number: '',
      isSelected: 0,
      isChecked: false,
      gender_data: [
        {label: 'Male', value: 0},
        {label: 'Female', value: 1},
      ],
      isLoading: false,
    };
  }
  open_terms_and_conditions=()=>{
Linking.openURL(`${parentUrl}2019-06-28T06-10-29.263ZTerms_and_Conditions.pdf`)

  }

  buttonStateHolder() {
    if (
      this.state.firstName != '' &&
      this.state.lastName != '' &&
      this.state.email != '' &&
      this.state.password != '' &&
      this.state.confirm_password != '' &&
      this.state.phone_number != '' &&
      this.state.isChecked != false
    ) {
      return false;
    } else {
      return true;
    }
  }

  navigate = (val, response) => {
    if (!val) {
      setTimeout(() => alert(response.message), 100);
      this.props.navigation.navigate('Login');
    }
  };

  validate() {
    var regex_name = /^[a-zA-Z]{1,}$/;
    var regex_email = /^[a-zA-Z]{1,}([.])?[a-zA-Z0-9]{1,}([!@#$%&_-]{1})?[a-zA-Z0-9]{1,}[@]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,3}([.]{1}[a-zA-Z]{2})?$/;
    var regex_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    var regex_phone_number = /^([|+|]{1}[|9|]{1}[|1|]{1})?[7698]{1}[0-9]{1,}$/;

    if (this.state.firstName == '') {
      this.refs.toast.show('Please enter your First Name!');
    } else if (this.state.firstName.length < 3) {
      this.refs.toast.show('First Name must have atleast 3 Characters!');
    } else if (!regex_name.test(this.state.firstName)) {
      this.refs.toast.show('First Name should contain characters only.');
    } else if (this.state.lastName == '') {
      this.refs.toast.show('Please enter your Last Name!');
    } else if (this.state.lastName.length < 3) {
      this.refs.toast.show('Last Name must have atleast 3 Characters!');
    } else if (!regex_name.test(this.state.lastName)) {
      this.refs.toast.show('Last Name should contain characters only.');
    } else if (this.state.email == '') {
      this.refs.toast.show('Please enter your Email ID!');
    } else if (!regex_email.test(this.state.email)) {
      this.refs.toast.show('Please enter a valid E-mail ID.');
    } else if (this.state.password == '') {
      this.refs.toast.show('Please enter your Password!');
    } else if (this.state.password.length < 8) {
      this.refs.toast.show('Password must have atleast 8 Characters!');
    } else if (!this.state.password.match(regex_password)) {
      this.refs.toast.show(
        'Password must contain atleast one number, special symbol, uppercase and lowercase letter.',
      );
    } else if (this.state.confirm_password == '') {
      this.refs.toast.show('Please confirm your Password!');
    } else if (this.state.confirm_password != this.state.password) {
      this.refs.toast.show('Passwords not matched.');
    } else if (this.state.phone_number == '') {
      this.refs.toast.show('Please enter your Phone Number!');
    } else if (!regex_phone_number.test(this.state.phone_number)) {
      this.refs.toast.show('Invalid Phone Number.');
    } else if (
      this.state.phone_number.length != 10 &&
      this.state.phone_number.length != 13
    ) {
      this.refs.toast.show('Phone Number must have 10 digits excluding +91.');
    } else if (this.state.isChecked == false) {
      this.refs.toast.show('Please agree the Terms and Condition!');
    } else {
      this.setState({isLoading: true});

      api
        .apiHttpRequest('register', 'POST', {
          email: this.state.email.toLowerCase(),
          pass: this.state.password,
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          phone_no: this.state.phone_number,
          confirmPass: this.state.confirm_password,
          gender: this.state.gender_data[this.state.isSelected].value,
        })
        .then(response => {
          this.setState({isLoading: false});

          if (response.status_code == 200) {
            this.setState({isLoading: false});
            this.navigate(this.state.isLoading, response);
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          this.refs.toast.show(error.message);
        });
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.flex_inner_middle}>
        <OfflineNotice/>
        <KeyboardAvoidingView enabled>
          <Toast ref="toast" />
          <ScrollView bounces={true}>
            <Loader loading={this.state.isLoading} />
            <StatusBar backgroundColor="#e91c1a" />

            <Text style={styles.middle_heading}>NeoSTORE</Text>
            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'First Name'}
                icon={'user-alt'}
                onChange={firstName => {
                  this.setState({firstName});
                }}
                value={this.state.firstName}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'Last Name'}
                icon={'user-alt'}
                onChange={lastName => {
                  this.setState({lastName});
                }}
                value={this.state.lastName}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'Email'}
                icon={'inbox'}
                onChange={email => {
                  this.setState({email});
                }}
                value={this.state.email}
                keyboardType="email-address"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'Password'}
                icon={'lock'}
                onChange={password => {
                  this.setState({password});
                }}
                value={this.state.password}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={true}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'Confirm Password'}
                icon={'lock'}
                onChange={confirm_password => {
                  this.setState({confirm_password});
                }}
                value={this.state.confirm_password}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={true}
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.radio_button_view}>
              <RadioForm formHorizontal={true}>
                <Text style={styles.gender_text}>Gender</Text>
                {this.state.gender_data.map((obj, i) => {
                  const {gender} = obj;
                  return (
                    <RadioButton key={i}>
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={this.state.isSelected === i}
                        onPress={value => {
                          this.setState({isSelected: value});
                        }}
                        borderWidth={1}
                        buttonInnerColor={'#fff'}
                        buttonOuterColor={'#fff'}
                        buttonSize={16}
                        buttonOuterSize={22}
                        buttonInnerSize={18}
                        buttonStyle={{}}
                        buttonWrapStyle={styles.radio_button_icon}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        labelHorizontal={true}
                        labelStyle={styles.radio_button_label}
                        onPress={value => {
                          this.setState({isSelected: value});
                        }}
                      />
                    </RadioButton>
                  );
                })}
              </RadioForm>
            </View>

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.register_input_view}
                text_input_style={styles.register_input_field}
                placeholder={'Phone Number'}
                icon={'mobile-alt'}
                onChange={phone_number => {
                  this.setState({phone_number});
                }}
                value={this.state.phone_number}
                keyboardType="numeric"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.check_box_view}>
              <Checkbox
                checkedComponent={
                  <Icon name="check-box" size={25} color="#fff" />
                }
                uncheckedComponent={
                  <Icon name="check-box-outline-blank" size={25} color="#fff" />
                }
                label=""
                labelStyle={styles.check_box_text}
                onChange={() => {
                  this.setState({isChecked: !this.state.isChecked});
                }}
                checked={this.state.isChecked}
              />
                <Text style={styles.text_terms_conditions}>
                  I agree the &nbsp;
                  <Text 
                  onPress={() => {
                    this.open_terms_and_conditions()
                  }}
                  style={styles.underline_text}>
                    Terms and Conditions.
                  </Text>
                </Text>
            </View>
            <View style={styles.vertical_margin}>
              <Button
                styleButton={styles.register_button}
                styleButton_text={styles.register_button_text}
                disabled={this.buttonStateHolder()}
                buttonState_color={
                  this.buttonStateHolder() ? '#A52A2A' : '#fff'
                }
                textState_color="#E91C1A"
                onPress={() => this.validate()}
                text="REGISTER"
                buttonStateHolder={this.buttonStateHolder()}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}
export default Register;
