import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  padding_bottom: {paddingBottom: 15},
  flex_outer: {
    backgroundColor: '#E91C1A',
    paddingHorizontal: '3%',
  },
  vertical_margin: {marginVertical: 15},
  radio_button_view: {
    marginTop:"2%",
    marginBottom:"5%",
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  top_heading: {
    fontSize: 25,
    color: '#fff',
  },

  flex_inner_middle: {
    paddingTop: '5%',
    backfaceVisibility: 'visible',
    alignItems: 'center',
    flex:1,
    backgroundColor:"#e91c1a"
  },

  register_input_view: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 58,
    borderColor: 'white',
    borderWidth: 1,
    paddingLeft: 10,
    alignSelf: 'center',
    width: '95%',
  },

  register_input_field: {
    width: '90%',
    height: 47,
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 16,
    paddingLeft: '5%',
    fontFamily:"Gotham-Medium"
  },

  middle_heading: {
    fontSize: 37,
    color: '#fff',
    marginBottom: '5%',
    paddingTop: '2%',
    fontFamily:"Gotham-Bold",
    alignSelf:"center"
  },

  register_input: {
    height: 55,
    paddingHorizontal: '2%',
    borderColor: 'white',
    borderWidth: 2,
    color: '#fff',
    width: '100%',
    fontSize: 16,
    marginTop: '4%',
  },

  radio_button_label: {
    fontSize: 16,
    color: '#fff',
    fontFamily:"Gotham-Medium"
  },

  radio_button_icon: {
    marginLeft: '5%',
    alignContent: 'flex-start',
  },


  confirm_password_input: {
    height: 55,
    paddingHorizontal: '2%',
    borderColor: 'white',
    borderWidth: 2,
    color: '#fff',
    width: '100%',
    fontSize: 16,
    marginTop: '4%',
    marginBottom: '5%',
  },
  modal_view: {
    backgroundColor: '#FFFFFF',
    height: 500,
    width: '95%',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    padding: 15,
    alignSelf: 'center',
  },
  modal_exit_button:{ alignSelf:"flex-end"},
  underline_text:{
    color: '#fff',
    fontFamily: 'Gotham-Bold',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  text_terms_conditions:{
    color: '#fff',
    fontFamily: 'Gotham-Medium',
    fontSize: 14,
  },
  gender_text: {
    fontSize: 18,
    color: '#ffffff',
    fontFamily:"Gotham-Medium",
    alignSelf:"center",
    paddingRight:10
  },
  check_box_view: {
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection:"row"
  },

  register_button: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    backgroundColor: '#fff',
    width: '95%',
    alignSelf: 'center',
  },

  phone_number_field: {
    height: 55,
    marginBottom: '5%',
    paddingHorizontal: '2%',
    borderColor: 'white',
    borderWidth: 2,
    color: '#fff',
    width: '100%',
    fontSize: 20,
    marginTop: '4%',
  },

  register_button_text: {
    fontSize: 21,
    color: '#E91C1A',
    paddingTop: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    fontFamily:"Gotham-Medium",

  },
  check_box_text: {
    color: '#fff',
    fontSize: 16,
    fontFamily:"Gotham-Medium"
  },
  check_box: {
    color: '#fff',
    marginTop: '0%',
  },
});
export default styles;
