import {StyleSheet} from "react-native"

const styles = StyleSheet.create({
    outer_safeareaview: {
      flex: 1,
    },
    lower_text: {flex: 0.4, justifyContent: 'center'},
    upper_text: {fontSize: 16,fontFamily:"Gotham-Book"},
    text_outer_view: {flex: 0.65, paddingVertical: 10},
    image_view: {
      flex: 0.35,
      alignItems: 'center',
      justifyContent: 'center',
    },
    upper_text_view: {flex: 0.6, justifyContent: 'center'},
    order_list: {
      height: 100,
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: '#bfbfbf',
    },
    order_image: {
      height: 80,
      width: 80,
    },
    total_text:{fontSize: 18, fontFamily:"Gotham-Medium"},
    lower_text_inner_view: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    category_text: {paddingTop: 5, fontFamily:"Gotham-Bookitalic", color: '#7f7f7f'},
    cost_font: {fontSize:14,color: '#7f7f7f', marginRight: 9,},
    quantity_text: {color: '#7f7f7f',fontFamily:"Gotham-Book"},
    total_text_view:{
      height: 85,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: '8%',
      alignItems: 'center',
      borderBottomWidth: 1,
      borderBottomColor: '#bfbfbf',
    }
  });

  export default styles;