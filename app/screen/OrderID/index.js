import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  StatusBar,
  SafeAreaView,
  Image,
} from 'react-native';
import CostINR from '../../components/cost_inr';
import styles from './styles';
import {ScrollView} from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import parentUrl from '../../components/parentUrl';
import OfflineNotice from "../../components/offlineNotice"

class OrderID extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_items: [],
      imageLoaded:false,
    };
  }
  componentDidMount() {
    this.setState({
      order_items: this.props.navigation.state.params.order_product
        .product_details,
    });
  }
  render_orders = () => {
    const {order_items} = this.state;
    return (
      Object.keys(order_items).length > 0 &&
      order_items.map((result, index) => {
        let cost_product =
          result.product_details[0].product_cost * result.quantity;
        return (
          <View style={styles.order_list}>
            <View style={styles.image_view}>
              <StatusBar backgroundColor="#e91c1a" />

              <Image
              onLoad={()=>{this.setState({imageLoaded:true})}}
                resizeMode="contain"
                style={styles.order_image}
                source={this.state.imageLoaded?{
                  uri: `${parentUrl}${result.product_details[0].product_image}`,
                }:require("../../assets/images.jpeg")}
              />
            </View>

            <View style={styles.text_outer_view}>
              <View style={styles.upper_text_view}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={1}
                  style={styles.upper_text}>
                  {result.product_details[0].product_name}
                </Text>
                <Text style={styles.category_text}>(Category)</Text>
              </View>
              <View style={styles.lower_text}>
                <View style={styles.lower_text_inner_view}>
                  <Text style={styles.quantity_text}>
                    QTY: {result.quantity}
                  </Text>
                  <Text style={styles.cost_font}>
                    <FontAwesome name="rupee" size={14} color="#7f7f7f" />
                    &nbsp;{CostINR(cost_product)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        );
      })
    );
  };

  render() {
    const {order_items} = this.state;
    return (
      Object.keys(order_items).length > 0 && (
        <ScrollView>
          <SafeAreaView style={styles.outer_safeareaview}>
            <OfflineNotice/>
            {this.render_orders()}
            <View style={styles.total_text_view}>
              <Text style={styles.total_text}>Total</Text>
              <Text style={styles.total_text}>
                <FontAwesome name="rupee" size={18} color="#333333" />
                &nbsp;{CostINR(order_items[0].total_cartCost)}
              </Text>
            </View>
          </SafeAreaView>
        </ScrollView>
      )
    );
  }
}
export default OrderID;
