import React, {Component} from 'react';
import {
  SafeAreaView,
  FlatList,
  ScrollView,
  View,
  StatusBar,
  Text,
  ActivityIndicator,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import api from '../../api';
import Loader from '../../components/loader';
import ProductCard from '../../components/productCard_render';
import styles from './styles';
import OfflineNotice from "../../components/offlineNotice"

class Tables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product_data: [],
      isLoading: true,
      refreshing: false,
      total_count: 0,
      count: 0,
      pageNo: 1,
      perPage: 6,
    };
  }
  arrayUnique = array => {
    var a = array.concat();
    a.reverse();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i]._id == a[j]._id) a.splice(j--, 1);
      }
    }

    return a;
  };
  fetch_data = () => {
    if (
      this.state.total_count > this.state.product_data.length ||
      this.state.total_count == 0
    ) {
      this.setState({isLoading: true});
      let product_list = [...this.state.product_data];
      // api.flag=true;

      api
        .apiHttpRequest('commonProducts', 'GET', {
          category_id: this.props.navigation.state.params._id,
          pageNo: this.state.pageNo,
          perPage: this.state.perPage,
        })
        .then(response => {
          this.setState({isLoading:false})
          if (
            response.status_code === 200 &&
            response.product_details.length > 0 &&
            typeof response.product_details == 'object'
          ) {
            this.setState({total_count: response.total_count});

            response.product_details.map((result, index) => {
              product_list.push(result);
            });
            product_list = this.arrayUnique(product_list);
            this.setState({product_data: product_list});
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          this.refs.toast.show(response.message);
        });
    } else {
      this.setState({isLoading: false});
      this.setState({pageNo: 1});
    }
  };
  componentWillUnmount() {
    this.setState({isLoading: false, pageNo: 1});
  }
  componentDidMount() {
    this.fetch_data();
  }
  re_Render = () => {
    alert('hello');
  };

  renderFooter = () => {
    return (
      <View style={styles.footer}>
        <View style={styles.loadMoreBtn}>
          <Text style={styles.btnText}>
            {this.state.product_data.length} of {this.state.total_count}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const {product_data, count} = this.state;
    if(product_data.length > 0 ){
    return (
        <SafeAreaView style={styles.outer_safeAreaView}>
          <Loader loading={this.state.isLoading}/>
          <OfflineNotice/>
          <View style={styles.outer_safeAreaView}>
            <StatusBar backgroundColor="#e91c1a" />

            <ScrollView>
              <SafeAreaView style={styles.outer_safeAreaView}>
                <FlatList
                  data={this.state.product_data}
                  renderItem={({item}) => {
                    return (
                      <ProductCard
                        data={item}
                        navigate={this.props.navigation}
                        re_Rende={this.re_Render}
                      />
                    );
                  }}
                  ListFooterComponent={() =>
                    this.state.isLoading && (
                      <ActivityIndicator size="large" color="#7f7f7f" />
                    )
                  }
                  keyExtractor={item => item.id}
                  onEndReached={() => {
                    this.setState(prevState => ({
                      pageNo: prevState.pageNo + 1,
                    }));

                    this.fetch_data();
                  }}
                  refreshing={this.state.isLoading}
                />
              </SafeAreaView>
            </ScrollView>
            {this.renderFooter()}
          </View>
        </SafeAreaView>
      
    );}
    else{
     return <OfflineNotice/>
    }
  }
}

export default Tables;
