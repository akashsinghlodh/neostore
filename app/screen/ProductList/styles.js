import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  outer_safeAreaView: {flex: 1},
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#282727df',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
    fontFamily: 'Gotham-Medium',
  },
});
export default styles;
