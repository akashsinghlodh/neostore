import React, {Component} from 'react';
import {
  View,
  Share,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import CostINR from '../../components/cost_inr';
import Loader from '../../components/loader';
import parentUrl from '../../components/parentUrl';
import ModalCustom from '../../components/modal';
import api from '../../api';
import InputField from '../../components/inputfield';
import Entypo from 'react-native-vector-icons/Entypo';
import StarRating from 'react-native-star-rating';
import Button from '../../components/button';
import styles from './styles';
import OfflineNotice from "../../components/offlineNotice"

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product_details_data: {},
      index: 0,
      modalVisible_buy_now: false,
      modalVisible_rate_now: false,
      product_quantity: 0,
      starCount: 0,
      isLoading: false,
      updateList: props.navigation.getParam('updateDetails', false),
      cart_data: [],
      cart_status: false,
      imageLoaded:false
    };
  }
  rate_api_hitting = () => {
    if (this.state.starCount > 0) {
      this.set_modalVisible_rate_now(!this.state.modalVisible_rate_now);
      api
        .apiHttpRequest(
          'updateProductRatingByCustomer',
          'PUT',
          {
            product_id: this.props.navigation.state.params.product_id,

            product_rating: this.state.starCount,
          },
          true,
        )
        .then(response => {
          this.setState({starCount: 0});
          this.refs.toast.show(response.message);
        })
        .catch(error => {
          this.refs.toast.show(error);
        });
    } else {
      this.refs.toast_ratenow.show('Please provide the ratings!');
    }
  };

  fetch_data = () => {
    this.setState({isLoading: true});

    api
      .apiHttpRequest('commonProducts', 'GET', {
        _id: this.props.navigation.state.params.product_id,
      })
      .then(response => {
        this.setState({isLoading: false});

        if (response.status_code == 200) {
          this.setState({product_details_data: response.product_details[0]});
        } else {
          this.refs.toast.show(response.message);
        }
      })
      .catch(error => console.log(error)
        // this.refs.toast.show(error.message)
      
      );
  };
  componentDidMount() {
    this.fetch_data();
  }
  componentWillUnmount() {
    this.props.navigation.state.params.re_Render;
  }
  componentDidUpdate(prevProps, prevState) {
    const currentProp = this.props.navigation.state.params.product_id;
    const prevProp = prevProps.navigation.state.params.product_id;
    if (currentProp !== prevProp) {
      this.fetch_data();
    }
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }
  set_modalVisible_buy_now = visible => {
    this.setState({modalVisible_buy_now: visible});
  };

  set_modalVisible_rate_now = visible => {
    this.setState({modalVisible_rate_now: visible});
  };

  onShare = async () => {
    Share.share(
      {
        title: 'Neostore',
        message: `Checkout out the latest offer ${this.state.product_details_data.product_name} in just RS. ${this.state.product_details_data.product_cost} only on NeoSTORE App. HURRY UP! Order Now \n\n http://180.149.241.208:3023/#/productDetails/${this.props.navigation.state.params.product_id}`,
        url: `${parentUrl}${this.state.product_details_data.subImages_id.product_subImages[0]}`,
      },
      {},
    );
  };

  validate = product_quantity => {
    this.setState({product_quantity});
  };
  product_images = () => {
    const {product_details_data} = this.state;
    return (
      <View style={styles.image_view}>
        <View style={styles.inner_view_images}>
          <Image
            resizeMode="contain"
            style={[
              styles.image_product_show,
              {opacity: product_details_data.product_stock == 0 ? 0.3 : 1},
            ]}
            source={this.state.imageLoaded?{
              uri: `${parentUrl}${Object.keys(this.state.product_details_data)
                .length > 0 &&
                product_details_data.subImages_id.product_subImages[
                  this.state.index
                ]}`,
            }:require("../../assets/images.jpeg")}
          />
        </View>

        <View style={styles.horizontal_image_slider}>
          <ScrollView horizontal={true}>
            {this.product_list(this.state.index)}
          </ScrollView>
        </View>
      </View>
    );
  };
  product_list = index_hover => {
    const {product_details_data} = this.state;
    return (
      Object.keys(product_details_data).length > 0 &&
      product_details_data.subImages_id.product_subImages.map(
        (result, index) => (
          <TouchableOpacity
            onPress={() => {
              this.setState({index: index});
            }}
            style={[
              styles.image_slider_horizontal,
              {
                borderWidth: index == index_hover ? 2 : 1,
                borderColor: index == index_hover ? 'red' : '#000',
              },
            ]}>
            <Image
            onLoad={()=>{this.setState({imageLoaded:true})}}
              resizeMode="contain"
              style={styles.image_slider_image}
              source={this.state.imageLoaded?{
                uri: `${parentUrl}${result}`,
              }:require("../../assets/images.jpeg")}
            />
          </TouchableOpacity>
        ),
      )
    );
  };
  product_stcok = val => {
    if (val == 0) {
      return 'Out of stock!';
    } else if (val == 1 || val == 2) {
      return 'Very few left';
    } else {
      return '';
    }
  };
  navigate_mycart = async () => {
    this.setState({cart_status: false});
    if (this.state.product_quantity != 0) {
      this.set_modalVisible_buy_now(!this.state.modalVisible_buy_now);
      const data = {
        _id: this.props.navigation.state.params.product_id,
        product_id: this.props.navigation.state.params.product_id,
        quantity: this.state.product_quantity,
      };
      let product = [];
      product.push(data);
      product.push({flag: 'logout'});
      api
        .apiHttpRequest(
          'addProductToCartCheckout',
          'POST',
          [...product],
          true,
          true,
        )
        .then(response => {
          if (response.status_code == 200) {
            this.setState({product_quantity: 0});
            this.props.navigation.navigate('MyCart', {
              qty: this.state.product_quantity,
            });
          } else {
            this.refs.toast.show(response.message);
          }
        })
        .catch(error => {
          this.refs.toast.show(error);
        });
    } else {
      this.refs.toast_buy_modal.show('Please enter a valid quantity!');
    }
  };
  render_modal_body_rate = () => {
    return (
      <View style={styles.modal_view}>
        <Toast ref="toast_ratenow" />
        <ScrollView style={styles.scrollview_modal}>
          <View style={styles.inner_view_modal_rate}>
            <Text style={styles.heading_modal}>
              {this.state.product_details_data.product_name}
            </Text>
            <Image
              borderWidth={1}
              borderColor={'#8e8e8e'}
              resizeMode="contain"
              style={styles.modal_image}
              source={{
                uri: `${parentUrl}${Object.keys(this.state.product_details_data)
                  .length > 0 &&
                  this.state.product_details_data.subImages_id
                    .product_subImages[0]}`,
              }}
            />
            <View style={styles.rate_star_view}>
              <StarRating
                disabled={false}
                maxStars={5}
                emptyStar={'star'}
                emptyStarColor={'#7f7f7f'}
                fullStarColor={'#ffba00'}
                halfStarColor={'#ffba00'}
                rating={this.state.starCount}
                selectedStar={rating => this.onStarRatingPress(rating)}
              />
            </View>
            <Button
              styleButton={styles.submit_button_modal}
              styleButton_text={styles.submit_button_modal_text}
              buttonState_color={'#e91b1a'}
              textState_color={'#fff'}
              onPress={() => {
                this.rate_api_hitting();
              }}
              text="RATE NOW"
            />
          </View>
        </ScrollView>
      </View>
    );
  };

  product_quantity_selection = product_quantity => {
    const int_regex = /^\d+$/;
    const quantity_max= this.state.product_details_data.product_stock<10?this.state.product_details_data.product_stock:10;
    if (product_quantity <quantity_max) {
      if (int_regex.test(product_quantity)) {
        this.refs.toast_buy_modal.close();
        this.setState({product_quantity});
      } else {
        this.refs.toast_buy_modal.show(
          'Invalid Quantity!',
          Toast.Duration.short,
          Toast.Position.center,
        );
        this.setState({product_quantity: 0});
      }
    } else {
      this.refs.toast_buy_modal.show(
        'Entered value exceeds the limit.',
        Toast.Duration.long,
        Toast.Position.center,
      );
      this.setState({product_quantity: 0});
    }
  };

  render_modal_body_buynow = () => {
    let data = [];
    let category = [];
    for (let i = 0; i < this.state.product_details_data.product_stock; i++) {
      data[i] = i + 1;
    }
    data.slice(0, this.state.product_details_data.product_stock).map(
      (result, index) =>
        (category[index] = {
          value: result,
        }),
    );
    return (
      <View style={styles.modal_view}>
        <Toast ref="toast_buy_modal" />

        <ScrollView style={styles.scrollview_modal}>
          <KeyboardAvoidingView behaviour="padding" enabled>
            <View style={styles.inner_view_modal_rate}>
              <Text style={styles.heading_modal}>
                {this.state.product_details_data.product_name}
              </Text>
              <Image
                borderWidth={1}
                borderColor={'#8e8e8e'}
                resizeMode="contain"
                style={styles.modal_image}
                source={{
                  uri: `${parentUrl}${Object.keys(
                    this.state.product_details_data,
                  ).length > 0 &&
                    this.state.product_details_data.subImages_id
                      .product_subImages[0]}`,
                }}
              />
              <Text style={styles.product_quantity}>Enter Qty.</Text>
              <InputField
                text_input_view_style={styles.order_modal}
                text_input_style={styles.order_modal_input_field}
                textAlign={'center'}
                onChange={product_quantity => {
                  this.product_quantity_selection(product_quantity);
                }}
                multiline={false}
                keyboardType="numeric"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
              />

              <Button
                styleButton={styles.submit_button_modal}
                styleButton_text={styles.submit_button_modal_text}
                buttonState_color={
                  this.state.product_quantity == 0 ? '#ededed' : '#e91b1a'
                }
                textState_color={
                  this.state.product_quantity == 0 ? '#000' : '#fff'
                }
                returnKeyType="done"
                onPress={() => {
                  this.navigate_mycart();
                }}
                text="SUBMIT"
              />
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  };

  render() {
    const catogory_id =
      Object.keys(this.state.product_details_data).length > 0 &&
      this.state.product_details_data;
if(Object.keys(this.state.product_details_data).length > 0){
    return (
      // Object.keys(this.state.product_details_data).length > 0 && (
        <SafeAreaView style={styles.safeareaview_outer}>
          <OfflineNotice/>
          <Toast ref="toast" />
          <Loader loading={this.state.isLoading} />
          <StatusBar backgroundColor="#e91c1a" />

          <View style={styles.safeareaview_outer}>
            <View style={styles.view_inner}>
              <Text style={styles.product_heading}>
                {this.state.product_details_data.product_name}
              </Text>
              <Text style={styles.product_category}>
                Category-
                {this.state.product_details_data.category_id.category_name}
              </Text>
              <View style={styles.product_data_row}>
                <Text style={styles.producer_text}>
                  {this.state.product_details_data.product_producer}
                </Text>
                <StarRating
                  disabled={true}
                  containerStyle={styles.rating}
                  starSize={16}
                  emptyStar={'star'}
                  emptyStarColor={'#4f4f4f'}
                  fullStarColor={'#FFBA00'}
                  halfStarColor={'#FFBA00'}
                  disabled={false}
                  maxStars={5}
                  rating={this.state.product_details_data.product_rating}
                  selectedStar={() => {
                    this.set_modalVisible_rate_now(true);
                  }}
                />
              </View>
            </View>

            <ModalCustom
              onBackdropPress={() => {
                this.setState({modalVisible_buy_now: false});
              }}
              visible={this.state.modalVisible_buy_now}
              body={this.render_modal_body_buynow()}
            />

            <ModalCustom
              onBackdropPress={() => {
                this.setState({modalVisible_rate_now: false});
              }}
              visible={this.state.modalVisible_rate_now}
              onRequestClose={() => {}}
              body={this.render_modal_body_rate()}
            />
            <ScrollView style={styles.scrollview}>
              <View style={styles.main_inner_view}>
                <View style={styles.product_cost_view}>
                  <Text style={styles.product_cost_text}>
                    Rs. {CostINR(this.state.product_details_data.product_cost)}
                  </Text>
                  <Text style={styles.product_stock_text}>
                    {this.product_stcok(
                      this.state.product_details_data.product_stock,
                    )}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.onShare();
                    }}>
                    <Entypo name="share" size={24} color={'#7f7f7f'}></Entypo>
                  </TouchableOpacity>
                </View>

                {this.product_images()}
                <View style={styles.description_view}>
                  <View>
                    <Text style={styles.description_heading}>DESCRIPTION:</Text>
                  </View>
                  <View>
                    <Text style={styles.description_text}>
                      {this.state.product_details_data.product_desc}
                    </Text>
                  </View>
                </View>
              </View>
            </ScrollView>

            <View style={styles.bottom_view}>
              <Button
                styleButton={styles.buy_now_button}
                styleButton_text={styles.buy_now_button_text}
                disabled={this.state.product_details_data.product_stock == 0}
                buttonState_color={
                  this.state.product_details_data.product_stock == 0
                    ? '#002A2A'
                    : '#e91b1a'
                }
                textState_color={
                  this.state.product_details_data.product_stock == 0
                    ? '#999'
                    : '#fff'
                }
                buttonStateHolder={
                  this.state.product_details_data.product_stock == 0
                }
                onPress={() => this.set_modalVisible_buy_now(true)}
                text="BUY NOW"
              />
              <Button
                styleButton={styles.rate_now_button}
                styleButton_text={styles.rate_now_button_text}
                disabled={false}
                buttonState_color="#ededed"
                textState_color="#7f7f7f"
                buttonStateHolder={
                  this.state.product_details_data.product_stock == 0
                }
                onPress={() => {
                  this.setState({starCount: 0}),
                    this.set_modalVisible_rate_now(true);
                }}
                text="RATE"
              />
            </View>
          </View>
        </SafeAreaView>
      )}
      
        return(          <Loader loading={this.state.isLoading} />
          )
      
    // );
  }
}
export default ProductDetails;
