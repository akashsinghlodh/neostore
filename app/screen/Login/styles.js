import {StyleSheet,Dimensions} from 'react-native';
const {height} = Dimensions.get('window');
const h=height-0.08*height;
const styles = StyleSheet.create({
  inner_safeareaview:{height:h,},
  plus_button_icon:{textAlign: 'center', justifyContent: 'center'},
  flex_apply: {
    backfaceVisibility: 'visible',
    alignItems: 'center',
    justifyContent:"center",
    backgroundColor:"#e91c1a"
  },
field_padding:{paddingBottom: "5%", },
  inner_flex_top: {
alignItems:"center",
    
    backgroundColor:"black"
    
  },

  heading: {
    fontFamily:"Gotham-Bold",
    fontSize: 40,
    paddingTop:"30%",
    color: '#fff',
    textAlign: 'center',
    paddingBottom: '10%',
  },

  display: {
    backgroundColor: '#E91C1A',
    width: '100%',
    height: '100%',
  },
login_input_view:{
  alignSelf:"center",
  flexDirection: 'row',
  alignItems: 'center',
width:"95%",
  height: 58,
  borderColor: 'white',
  borderWidth:1,
  paddingLeft: 10,
},

  login_input: {
    width:"92%",
    height: 47,
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 16,
    paddingLeft: "5%",
    fontFamily:"Gotham-Medium"

  },

  login_button: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    color: '#E91C1A',
    backgroundColor: '#fff',
    width:"95%",
    alignSelf:"center"
  },

  button_text: {
    paddingTop:5,
    fontSize: 21,
    color: '#E91C1A',
    fontFamily:"Gotham-Medium"

  },

  forget_password_text: {
    marginTop: '5%',
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
    fontFamily:"Gotham-Medium"

  },

  no_account_field: {
    flex: 0.9,
    justifyContent: 'center',
    
  },
  plus_icon: {
    height: 53,
    width:53,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#9e0100',
  },
  no_account_text: {
    color: '#fff',
    fontSize: 15,
    fontFamily:"Gotham-Medium"

  },
  inner_flex_bottom: {
    height:70,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
      bottom:0,
     position:"absolute",
     width:"100%"
  },
});

export default styles;
