import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';
import Toast from 'react-native-whc-toast';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../../components/button';
import InputField from '../../components/inputfield';
import Loader from '../../components/loader';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';
import api from '../../api';
import OfflineNotice from "../../components/offlineNotice"
import Entypo from 'react-native-vector-icons/Entypo';
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user_name: 'akashsingh@neosofttech.com',
      password: 'Akash1212@',
      isLoading: false,
    };
  }

  navigate_to = (val, response) => {
    if (!val) {
      this.refs.toast.show(response.message);
      setTimeout(
        () => this.props.navigation.navigate('Home', {title: 'NeoSTORE'}),
        500,
      );
    }
  };
  validate_login_credentials() {
    if (this.state.user_name == '') {
      this.refs.toast.show('Please enter Username!');
    } else if (this.state.password == '') {
      this.refs.toast.show('Please enter Password!');
    } else {
      this.setState({isLoading: true});
api.flag=true;
      api
        .apiHttpRequest('login', 'POST', {
          email: this.state.user_name.toLowerCase(),
          pass: this.state.password,
        })
        .then(response => {
          this.setState({isLoading: false});
          if (response.status_code == 200) {
            AsyncStorage.setItem('token', response.token);
            AsyncStorage.setItem('cart_count', `${response.cart_count}`);
            AsyncStorage.setItem(
              'first_name',
              response.customer_details.first_name,
            );
            AsyncStorage.setItem(
              'last_name',
              response.customer_details.last_name,
            );

            this.navigate_to(this.state.isLoading, response);
          } else {
            if (!this.state.isLoading) {
              this.refs.toast.show(response.message);
            }
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          // this.refs.toast.show(error.message);
        });
    }
  }
  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('cart_count');
    } catch (error) {}
  };

  render() {
    return (
      <SafeAreaView style={styles.flex_apply}>
                <OfflineNotice /> 

        <KeyboardAvoidingView enabled>
          <Toast ref="toast" />
          <ScrollView bounces={true}>
            <SafeAreaView style={styles.inner_safeareaview}>
              <Loader loading={this.state.isLoading} />
              <StatusBar backgroundColor="#e91c1a" />

              <Text style={styles.heading}>NeoSTORE</Text>

              <View style={styles.field_padding}>
                <InputField
                  text_input_view_style={styles.login_input_view}
                  text_input_style={styles.login_input}
                  placeholder={'Username'}
                  icon={'user-alt'}
                  onChange={user_name => {
                    this.setState({user_name});
                  }}
                  value={this.state.user_name}
                  keyboardType="email-address"
                  secureTextEntry={false}
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.field_padding}>
                <InputField
                  text_input_view_style={styles.login_input_view}
                  text_input_style={styles.login_input}
                  placeholder={'Password'}
                  icon={'lock'}
                  onChange={password => {
                    this.setState({password});
                  }}
                  value={this.state.password}
                  keyboardType="default"
                  secureTextEntry={true}
                  blurOnSubmit={false}
                />
              </View>

              <Button
                styleButton={styles.login_button}
                styleButton_text={styles.button_text}
                disabled={false}
                buttonState_color="#fff"
                textState_color="#E91C1A"
                buttonStateHolder={false}
                onPress={() => this.validate_login_credentials()}
                text="LOGIN"
              />
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ForgetPassword');
                }}>
                <Text style={styles.forget_password_text}>
                  Forget Password ?
                </Text>
              </TouchableOpacity>

              <View style={styles.inner_flex_bottom}>
                <View style={styles.no_account_field}>
                  <Text style={styles.no_account_text}>
                    DONT HAVE AN ACCOUNT ?
                  </Text>
                </View>

                <TouchableOpacity
                  style={styles.plus_icon}
                  onPress={() => {
                    this.props.navigation.navigate('Register');
                  }}>
                  <Text style={styles.plus_button_icon}>
                    <Entypo name="plus" size={35} color={'#fff'} />
                  </Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

export default Login;
