import {StyleSheet} from "react-native"
const styles = StyleSheet.create({
    order_id_view: {
      flex: 0.55,
      justifyContent: 'center',
      borderBottomColor: '#ededed',
      borderBottomWidth: 1,
    },
    order_date_view: {flex: 0.45, justifyContent: 'center'},
    outer_most_view: {
      flex: 1,
      paddingHorizontal: 0,
    },
    inner_safeareaview: {},
    inner_view: {
      height: 91,
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: '#bfbfbf',
      paddingHorizontal: 10,
    },
    order_data: {
      flex: 0.6,
      justifyContent: 'center',
    },
    order_cost: {
      flex: 0.4,
      alignItems: 'flex-end',
      justifyContent: 'center',
    
    },
    order_date: {
      color: '#4f4f4f',
      fontFamily:"Gotham-Book"

    },
    order_cost_text: {
      fontSize: 16,
      color: '#333333',
      fontFamily:"Gotham-Book"

    },
    order_id: {
      color: '#1c1c1c',
      fontSize: 16,
      fontFamily:"Gotham-Book",
      paddingTop:5
    },
  });
export default styles;  