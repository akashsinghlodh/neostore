import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import styles from './styles';
import Loader from '../../components/loader';
import Toast from 'react-native-whc-toast';
import CostINR from "../../components/cost_inr"
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import OfflineNotice from "../../components/offlineNotice"

import api from '../../api';
class MyOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_data: [],
      isLoading: false,
    };
  }
  componentDidMount() {
    this.setState({isLoading: true});

    api
      .apiHttpRequest('getOrderDetails', 'GET', {}, true)
      .then(response => {
        this.setState({isLoading: false});
        if (response.status_code == 200) {
          this.setState({order_data: response.product_details});
        } else {
          this.refs.toast.show(response.message);
        }
      })
      .catch(error => {
        this.refs.toast.show(error.message);
      });
  }
  render_order = () => {
    const {order_data} = this.state;
    return (
      Object.keys(order_data).length > 0 &&
      order_data.map((result, index) => {
        return (
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('OrderID', {
                title: `Order ID: ${result._id.slice(4)}`,
                order_product: result,
              });
            }}
            style={styles.inner_view}>
            <View style={styles.order_data}>
              <View style={styles.order_id_view}>
                <Text style={styles.order_id}>
                  Order ID: {result._id.slice(4)}
                </Text>
              </View>

              <View style={styles.order_date_view}>
                <Text style={styles.order_date}>
                  Order date: {result.product_details[0].createdAt.slice(0, 10)}
                </Text>
              </View>
            </View>
            <View style={styles.order_cost}>
              <Text style={styles.order_cost_text}>
                <FontAwesome name="rupee" size={18} color="#4f4f4f" />
                &nbsp;{CostINR(result.product_details[0].total_cartCost)}
                 </Text>
            </View>
          </TouchableOpacity>
        );
      })
    );
  };
  render() {
    return (
      <SafeAreaView style={styles.inner_safeareaview}>
        <OfflineNotice/>
        <Toast ref="toast" />
        <StatusBar backgroundColor="#e91c1a" />

        <ScrollView>
          <Loader loading={this.state.isLoading} />
          {this.render_order()}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default MyOrders;
