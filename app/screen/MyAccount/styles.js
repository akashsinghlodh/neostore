import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  outer_safeareaview: {flex: 1, backgroundColor: '#E91C1A'},
  inner_view: {flex: 1, alignItems: 'center', padding: 15},
  image_dp: {height: 150, width: 150, borderRadius: 75, marginBottom: 15},
  padding_bottom: {paddingBottom: 15},
  reset_button_view: {backgroundColor: '#fff', height: 60},
  edit_profile_view: {
    width: '95%',
    flexDirection: 'row',
    alignItems: 'center',
    height: 55,
    borderColor: 'white',
    borderWidth: 1,
    paddingLeft: 10,
    paddingHorizontal: 5,
  },

  edit_profile_view_text: {
    width: '90%',
    height: 40,
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 15,
    paddingLeft: '5%',
    fontFamily: 'Gotham-Medium',
  },
  edit_profile_button: {
    width: '95%',
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    color: '#E91C1A',
    backgroundColor: '#fff',
  },

  edit_profile_button_text: {
    fontSize: 21,
    color: '#E91C1A',
    paddingTop: 5,
    fontFamily: 'Gotham-Medium',
  },

  reset_password_button: {
    width: '95%',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },

  reset_password_button_text: {
    fontSize: 21,
    color: '#E91C1A',
    fontFamily: 'Gotham-Medium',
    paddingTop: 5,
  },
  button_padding_bottom: {
    paddingBottom: 15,
    width: '100%',
    alignItems: 'center',
  },
});
export default styles;
