import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Image, StatusBar} from 'react-native';
import Toast from 'react-native-whc-toast';
import InputField from '../../components/inputfield';
import Button from '../../components/button';
import styles from './styles';
import api from '../../api';
import Loader from '../../components/loader';
import parentUrl from '../../components/parentUrl';
import OfflineNotice from "../../components/offlineNotice"

class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      customer_data: {},
    };
  }

  componentDidMount() {
    this.setState({isLoading: true});

    api
      .apiHttpRequest('getCustProfile', 'GET', {}, true)
      .then(response => {
        this.setState({isLoading: false});
        if (response.status_code == 200) {
          this.setState({
            customer_data: response.customer_proile,
          });
        } else {
          this.refs.toast.show(response.message);
        }
      })
      .catch(error => error);
  }
  render() {
    return (
      <SafeAreaView style={styles.outer_safeareaview}>
        <OfflineNotice/>
        <Toast ref="toast" />
        <Loader loading={this.state.isLoading} />
        <StatusBar backgroundColor="#e91c1a" />

        <ScrollView>
          <View style={styles.inner_view}>
            <Image
              style={styles.image_dp}
              source={
                this.state.customer_data.profile_img != null
                  ? {uri: `${parentUrl}${this.state.customer_data.profile_img}`}
                  : {
                      uri:
                        'https://yt3.ggpht.com/a-/AAuE7mDaG2knmVyICW5ENEMk2tVgDPvMagd6wk0xww=s88-c-k-c0x00ffffff-no-rj-mo',
                    }
              }
            />

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'user'}
                value={this.state.customer_data.first_name}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
                editable={false}
              />
            </View>

            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'user'}
                value={this.state.customer_data.last_name}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
                editable={false}
              />
            </View>
            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'envelope-open-text'}
                value={this.state.customer_data.email}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
                editable={false}
              />
            </View>
            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'mobile-alt'}
                value={`${this.state.customer_data.phone_no}`}
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
                editable={false}
              />
            </View>
            <View style={styles.padding_bottom}>
              <InputField
                text_input_view_style={styles.edit_profile_view}
                text_input_style={styles.edit_profile_view_text}
                icon={'birthday-cake'}
                value={
                  this.state.customer_data.dob != 'null' &&
                  this.state.customer_data.dob != null
                    ? this.state.customer_data.dob
                    : 'dd-mm-yyyy'
                }
                keyboardType="default"
                returnKeyType={'next'}
                secureTextEntry={false}
                blurOnSubmit={false}
                editable={false}
              />
            </View>
            <View style={styles.button_padding_bottom}>
              <Button
                styleButton={styles.edit_profile_button}
                styleButton_text={styles.edit_profile_button_text}
                disabled={false}
                buttonState_color="#fff"
                textState_color="#E91C1A"
                buttonStateHolder={false}
                onPress={() => {
                  this.props.navigation.navigate('EditProfile');
                }}
                text="EDIT PROFILE"
              />
            </View>
          </View>
        </ScrollView>
        <View style={styles.reset_button_view}>
          <Button
            styleButton={styles.reset_password_button}
            styleButton_text={styles.reset_password_button_text}
            disabled={false}
            buttonState_color="#fff"
            textState_color="#7f7f7f"
            buttonStateHolder={false}
            onPress={() => {
              this.props.navigation.navigate('ResetPassword');
            }}
            text="RESET PASSWORD"
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default MyAccount;
