import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import parentUrl from './components/parentUrl';
import NetInfo from '@react-native-community/netinfo';
// import AlertView from "./components/alert";
const base_url = parentUrl;
const transformObj = data => {
  let str = '';
  let updatedStr;
  Object.keys(data).map(result => {
    str = str.concat(`${result}=${data[result]}`, '&');
  });
  updatedStr = str.slice(0, -1);
  return updatedStr;
};
let netStatus = false;
const api = {
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },

  requestHeader_json: {
    'Content-Type': 'application/json',
  },
  handleConnectivityChange: function(isConnected) {
    netStatus = isConnected;
  },
  apiHttpRequest: async function(
    url,
    type,
    data,
    isSecure = false,
    isJson = false,
  ) {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange,
    );
    if (netStatus) {
      const token = await AsyncStorage.getItem('token');
      if (type == 'POST' || type == 'PUT') {
        const url_fetch = `${base_url}${url}`;
        return fetch(url_fetch, {
          method: type,
          mode: 'cors',
          cache: 'no-cache',
          body: isJson ? JSON.stringify(data) : transformObj(data),
          credentials: 'same-origin',

          headers: isJson
            ? isSecure
              ? {...this.requestHeader_json, Authorization: `bearer ${token}`}
              : this.requestHeader_json
            : isSecure
            ? {...this.requestHeader, Authorization: `bearer ${token}`}
            : this.requestHeader,
        })
          .then(res => res.json())
          .catch(error => error);
      } else {
        const url_fetch = `${base_url}${url}?${transformObj(data)}`;
        return fetch(url_fetch, {
          method: type,
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: isJson
            ? isSecure
              ? {...this.requestHeader_json, Authorization: `bearer ${token}`}
              : this.requestHeader_json
            : isSecure
            ? {...this.requestHeader, Authorization: `bearer ${token}`}
            : this.requestHeader,
        })
          .then(res => res.json())
          .catch(error => error);
      }
    }
  },
};
export default api;
