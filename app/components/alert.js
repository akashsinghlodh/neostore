import {Alert} from "react-native"

function AlertView(title, message, onSuccess) { 
   return( Alert.alert(
                title,
                message,
                [
                  {
                    text: 'Cancel',
                    onPress: () => false,
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => onSuccess()},
                ],
                {cancelable: true},))
           
           
 }
 export default AlertView;