import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, SafeAreaView, ScrollView,StatusBar} from 'react-native';
import InputField from './inputfield';
import api from '../api';
class ModalScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search_data: [],
      search:""
    };
  }
  listing_suggestions = () => {
    const {search_data} = this.state;
    if((typeof search_data)=="object"){
    return (
      Object.keys(search_data).length > 0 &&
      search_data.map(result => {
        // console.log("test",search_data)
        return (
                <TouchableOpacity 
          onPress={()=>{
              this.props.navigation.navigate("ProductDetails",{product_id: result.product_id,title:result.product_name})
          }}
          style={styles.touch}>
            <Text style={styles.text_style}>{result.product_name}</Text>
          </TouchableOpacity>

        );
      })
    )}
    else{
      return(
        <View style={styles.no_result_view}>
          <Text style={styles.no_result_text_style}>No Result Found!</Text>
        </View>
      )
    }
  };

  Search_item = search => {
    api
      .apiHttpRequest('commonProducts', 'GET', {name: search})
      .then(response => {
      //   console.log(response);
      //  console.log((typeof response.product_details)=="object")
        this.setState({search_data: response.product_details});
      })
      .catch(error => console.log(error));
  };
  render() {
    return (
      <View
        style={styles.outer_view}>
        <SafeAreaView>
        <StatusBar backgroundColor="#e91c1a" />

          <InputField
                  numberOfLines={1}

            text_input_view_style={styles.search_input_view}
            text_input_style={styles.search_input}
            placeholder={'Search'}
            icon="search"
            onChange={search => {
              this.Search_item(search);
              this.setState({search})
            }}
            value={this.state.search}
            keyboardType="default"
            secureTextEntry={false}
            blurOnSubmit={false}
            isRightIcon={this.state.search!=""?true:false}
            Righticon="x"
            onRightIconPress={() => {
              this.setState({search:""})
            }}
          />
         <ScrollView>          

          {this.listing_suggestions()}
          </ScrollView>          

        </SafeAreaView>
      </View>
    );
  }
}
export default ModalScreen;
const styles = StyleSheet.create({
  search_input_view: {
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    width: '95%',
    height: 58,
    borderColor: 'white',
    borderWidth: 1,
    paddingLeft: 10,
    backgroundColor: '#00000040',
  },
  no_result_view:{flex:1, alignItems:"center", justifyContent:"center",paddingTop:"5%"},
text_style:{fontSize:18, color:"#fff"},
no_result_text_style:{fontSize:30, color:"#fff", fontFamily:"Gotham-Medium"},
  search_input: {
    width: '85%',
    height: 47,
    color: '#fff',
    fontSize: 19,
    paddingLeft: '5%',
  },
  touch:{height:55, borderBottomWidth:1,borderBottomColor:"#fff", justifyContent:"center",},
  outer_view:{
    flex: 1,
    alignItems: 'center',
    paddingVertical:"5%",
    backgroundColor:"#e91c1a"
  }
});
