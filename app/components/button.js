import React, { Component } from 'react'
import { Text, TouchableOpacity} from "react-native"
class Button extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }
    render(){
     return(
        
        <TouchableOpacity
        disabled={this.props.disabled}
        style={[this.props.styleButton,{ backgroundColor: this.props.buttonState_color }]}
        onPress={() => this.props.onPress()}>
        <Text style={[this.props.styleButton_text,{color:this.props.textState_color}]}>{this.props.text}</Text>
      </TouchableOpacity>
     )   
    }
}

export default Button;