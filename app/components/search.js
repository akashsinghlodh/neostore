import React,{Component} from "react"
import {View, Text, TouchableOpacity} from "react-native"
import { SearchBar } from 'react-native-elements';
import ModalCustom from "./modal"
class Search extends Component{
    constructor(props){
        super(props)
        this.state={
search:""
        }
    }
    search_bar=()=>{
       return( <SearchBar
        placeholder="Type Here..."
        onChangeText={(search)=>{
this.setState({search})
        }}
        value={this.state.search}
      />)
    }

render(){
    return(
        <View>
            <ModalCustom
            isVisible={this.props.isVisible}
body={this.search_bar()}
            >

            </ModalCustom>
        </View>
    )
}
}

export default Search;