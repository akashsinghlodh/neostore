import React, {Component} from 'react';
import {View, TextInput, StyleSheet, Keyboard} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from "react-native-vector-icons/Feather"
class InputField extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    //console.log(this.props)
    return (
      <View
        style={this.props.text_input_view_style}>
        <FontAwesome5 name={this.props.icon} size={16} color={'#fff'} />
        <TextInput
        // returnKeyType="next"
        onSubmitEditing={()=>{this.props.multiline?false:Keyboard.dismiss()}}
        editable={this.props.editable}
        textAlign={this.props.textAlign?this.props.textAlign:'left'}
        numberOfLines={this.props.numberOfLines?this.props.numberOfLines:1}

          style={this.props.text_input_style}
          placeholderTextColor="#fff"
          placeholder={this.props.placeholder}
          multiline={this.props.multiline?this.props.multiline:false}
          onChangeText={value => {
            this.props.onChange(value);
          }}
          value={this.props.value?this.props.value:null}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnType}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
        />
        {
        this.props.isRightIcon ? 
      <Feather 
      onPress={()=>{
        this.props.onRightIconPress()
      }}
      name={this.props.Righticon} size={19} color={'#fff'} />:null 
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flex_apply: {
    flex: 1,
    height: '100%',
    paddingHorizontal: '5%',
    backgroundColor: '#E91C1A',
    height: '100%',
  },

  inner_flex_top: {
    flex: 0.9,

    justifyContent: 'center',
  },

  heading: {
    fontSize: 45,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    paddingBottom: '10%',
  },

  display: {
    backgroundColor: '#E91C1A',
    width: '100%',
    height: '100%',
  },

  login_input: {
    width: '92%',
    height: 47,
    backgroundColor: '#E91C1A',
    color: 'white',
    fontSize: 19,
    paddingLeft: '5%',
  },

  login_button: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    color: '#E91C1A',
    backgroundColor: '#fff',
  },

  button_text: {
    fontSize: 30,
    color: '#E91C1A',
  },

  forget_password_text: {
    marginTop: '5%',

    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },

  no_account_field: {
    flex: 0.9,
    justifyContent: 'center',
  },
  plus_icon: {
    flex: 0.2,
    height: 53,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#9e0100',
  },
  no_account_text: {
    color: '#fff',
    fontSize: 15,
  },
  inner_flex_bottom: {
    flex: 0.1,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
  },
});
export default InputField;
