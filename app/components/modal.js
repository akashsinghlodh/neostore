import React, {Component} from 'react';
import Modal from "react-native-modal"
class ModalCustom extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
  isVisible={this.props.visible}
  onBackdropPress={() =>{this.props.onBackdropPress()}}
  onBackButtonPress	={()=>{this.props.onBackdropPress()}}
>
{this.props.body}
</Modal>
        
      // <Modal
      //   animationType="none"
      //   transparent={true}
      //   visible={this.props.visible}
      //   onRequestClose={() => {}}>
      //       {this.props.body}
      //   </Modal>
      
    );
  }
}

export default ModalCustom;
