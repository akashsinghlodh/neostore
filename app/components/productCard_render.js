import React,{ useState } from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import StarRating from 'react-native-star-rating';
import parentUrl from './parentUrl';
import CostINR from './cost_inr';
function ProductCard(props) {
  // console.log("props",props)
  const {data} = props;
  //props.loader(false)
  const [imageloading, setImageLoading] = useState(false);
  return (
    <TouchableOpacity
      style={styles.touchable_outer}
      onPress={() => {
        props.navigate.navigate('ProductDetails', {
          product_id: data.product_id,
          title: data.product_name,
          re_Render: props.re_Render,
        });
      }}>
      <View style={styles.view_outer}>
        <View>
          <Image
            resizeMode="contain"
            onLoad={() => {
              setImageLoading(true)
            }}
            style={styles.image_shown}
            source={imageloading?{
              uri: `${parentUrl}${data.product_image}`,
            }:require('../assets/images.jpeg')}
            ></Image>
        </View>
        <View style={styles.inner_view}>
          <View style={styles.heading_view}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={styles.heading_text}>
              {data.product_name}
            </Text>
            <Text style={styles.producer_data}>{data.product_producer}</Text>
          </View>
          <View style={styles.cost_view}>
            <Text style={styles.cost_text}>
              Rs. {CostINR(data.product_cost)}
              {/* Rs. {parseFloat(data.product_cost).toLocaleString()} */}
            </Text>
            <StarRating
              containerStyle={styles.star_style}
              starSize={14}
              emptyStarColor={'#7f7f7f'}
              fullStarColor={'#ffba00'}
              halfStarColor={'#ffba00'}
              disabled={true}
              maxStars={5}
              rating={data.product_rating}
              emptyStar={'star'}
              halfStar={'star-half-full'}
            />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default ProductCard;

const styles = StyleSheet.create({
  star_style: {alignSelf: 'center'},
  cost_text: {
    flex: 1.5,
    fontSize: 18,
    color: 'red',
    alignSelf: 'center',
    fontFamily: 'Gotham-Medium',
  },
  cost_view: {flex: 2, flexDirection: 'row'},
  producer_data: {color: '#4f4f4f', fontSize: 12, fontFamily: 'Gotham-Book'},
  heading_text: {color: '#4f4f4f', fontFamily: 'Gotham-Medium', fontSize: 16},
  heading_view: {flex: 2, justifyContent: 'space-evenly'},
  inner_view: {flex: 1},
  touchable_outer: {
    borderBottomColor: '#ededed',
    borderBottomWidth: 1,
  },
  view_outer: {
    padding: 10,
    flexDirection: 'row',
  },
  container: {
    flex: 1,
  },
  image_shown: {
    height: 90,
    width: 90,
    marginRight: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
  },
  title: {
    fontSize: 32,
  },
});
