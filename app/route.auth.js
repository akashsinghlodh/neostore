
import { createStackNavigator } from 'react-navigation-stack';
import Login from "./screen/Login";
import Register from './screen/Register';
import ForgetPassword from './screen/Forget_Password'
import ForgetPasswordOTP from './screen/ForgetPassword_otp'

const AuthStack = createStackNavigator({
    Login: {
      screen: Login,
      navigationOptions: ({}) => ({
        header: null,
      }),
    },
    Register: {
      screen: Register,
      navigationOptions: ({ }) => ({
          title: 'Register'
      })
  },
  ForgetPassword: {
      screen: ForgetPassword,
  },
  ForgetPasswordOTP: {
    screen: ForgetPasswordOTP,
},
 
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#E91C1A',
        fontSize: 20,
      },
      headerTitleAllowFontScaling: true,
      headerTintColor: '#fff',
      headerTitleAllowFontScaling: true,
      headerTitleStyle: {
        fontSize: 20,
        fontFamily:"Gotham-Medium"

      },
    },
  });
export default AuthStack;