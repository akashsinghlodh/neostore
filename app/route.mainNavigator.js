import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthStack from './route.auth';
import Login from './screen/Login';
import Register from './screen/Register';
import ForgetPassword from './screen/Forget_Password';
import ProductDetails from './screen/Product_Details';
import Home from './screen/Home';
import MainNavigator from "./route.mainNavigator"

import MyCart from './screen/MyCart';
import Tables from './screen/Tables';
import Sofas from './screen/Sofas';
import Chairs from './screen/Chairs';
import Cupboards from './screen/Cupboards';
import MyAccount from './screen/MyAccount';
import StoreLocator from './screen/StoreLocator';
import MyOrders from './screen/MyOrders';
import Logout from './screen/Logout';
import CustomDrawerContentComponent from './drawerComponent';
import ResetPassword from './screen/ResetPassword';
import AntDesign from 'react-native-vector-icons/AntDesign';

const AppNavigator = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions:({ })=>({
header: null
        })
    }, 
    ResetPassword:{
        screen:ResetPassword,
        navigationOptions:({ })=>({
            title:"ResetPassword"
        })
    },
    MyCart: {
        screen: MyCart,
      navigationOptions:({ })=>({
        header: null,
        drawerIcon: () => <AntDesign name ="shoppingcart" size ={15} color ={"#fff"}/>
                })
            },
    Tables: {
        screen: Tables,
        navigationOptions:({ })=>({
            title: 'Tables',
            headerStyle: {
              backgroundColor: '#E91C1A',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
              fontSize:30,

            },
          
        })
    },
    Sofas: {
        screen: Sofas,
        navigationOptions:({ })=>({
header: null
        })
    },
    Chairs: {
        screen: Chairs,
        navigationOptions:({ })=>({
header: null
        })
    },
    Cupboards: {
        screen: Cupboards,
        navigationOptions:({ })=>({
            header: null
        })
    },
    MyAccount: {
        screen: MyAccount,
        navigationOptions:({ })=>({
        header: null
        })
    },
    StoreLocator: {
        screen: StoreLocator,
        navigationOptions:({ })=>({
header: null
        })
    },
    MyOrders: {
        screen: MyOrders,
        navigationOptions:({ })=>({
header: null,

        })
    },
    Logout: {
        screen: Logout,
        navigationOptions:({ })=>({
header: null
        })
    },
    ProductDetails: {
        screen: ProductDetails,
    }
})

const MainNavigator = createDrawerNavigator({
    screening:AppNavigator,
    MyCart: MyCart,
    Tables:Tables,
    Sofas: Sofas,
Chairs:Chairs,
Cupboards:Cupboards,
MyAccount:MyAccount,
StoreLocator:StoreLocator,
MyOrders:MyOrders,
Logout:Logout,
}, {
    drawerBackgroundColor:"#fff",
    edgeWidth:200,
    drawerType:"slide",
  activeTintColor:"#fff",
  contentComponent:CustomDrawerContentComponent,
  contentOptions: {
    inactiveTintColor:"#fff",
    activeTintColor: '#fff',
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1
    }
  }
})
export default MainNavigator;